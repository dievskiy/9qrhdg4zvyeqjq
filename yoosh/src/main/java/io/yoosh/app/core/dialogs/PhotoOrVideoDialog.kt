/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.core.dialogs

import android.app.Activity
import androidx.core.view.isVisible
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import io.yoosh.app.R
import io.yoosh.app.databinding.DialogPhotoOrVideoBinding
import io.yoosh.app.features.settings.AppPreferences

class PhotoOrVideoDialog(
        private val activity: Activity,
        private val vectorPreferences: AppPreferences
) {

    interface PhotoOrVideoDialogListener {
        fun takePhoto()
        fun takeVideo()
    }

    interface PhotoOrVideoDialogSettingsListener {
        fun onUpdated()
    }

    fun show(listener: PhotoOrVideoDialogListener) {
        when (vectorPreferences.getTakePhotoVideoMode()) {
            AppPreferences.TAKE_PHOTO_VIDEO_MODE_PHOTO -> listener.takePhoto()
            AppPreferences.TAKE_PHOTO_VIDEO_MODE_VIDEO -> listener.takeVideo()
            /* VectorPreferences.TAKE_PHOTO_VIDEO_MODE_ALWAYS_ASK */
            else                                       -> {
                val dialogLayout = activity.layoutInflater.inflate(R.layout.dialog_photo_or_video, null)
                val views = DialogPhotoOrVideoBinding.bind(dialogLayout)

                // Always default to photo
                views.dialogPhotoOrVideoPhoto.isChecked = true

                MaterialAlertDialogBuilder(activity)
                        .setTitle(R.string.option_take_photo_video)
                        .setView(dialogLayout)
                        .setPositiveButton(R.string._continue) { _, _ ->
                            submit(views, vectorPreferences, listener)
                        }
                        .setNegativeButton(R.string.cancel, null)
                        .show()
            }
        }
    }

    fun showPhoto(listener: PhotoOrVideoDialogListener) {
        listener.takePhoto()
    }

    private fun submit(views: DialogPhotoOrVideoBinding,
                       vectorPreferences: AppPreferences,
                       listener: PhotoOrVideoDialogListener) {
        val mode = if (views.dialogPhotoOrVideoPhoto.isChecked) {
            AppPreferences.TAKE_PHOTO_VIDEO_MODE_PHOTO
        } else {
            AppPreferences.TAKE_PHOTO_VIDEO_MODE_VIDEO
        }

        when (mode) {
            AppPreferences.TAKE_PHOTO_VIDEO_MODE_PHOTO -> listener.takePhoto()
            AppPreferences.TAKE_PHOTO_VIDEO_MODE_VIDEO -> listener.takeVideo()
        }
    }

    fun showForSettings(listener: PhotoOrVideoDialogSettingsListener) {
        val currentMode = vectorPreferences.getTakePhotoVideoMode()

        val dialogLayout = activity.layoutInflater.inflate(R.layout.dialog_photo_or_video, null)
        val views = DialogPhotoOrVideoBinding.bind(dialogLayout)

        // Always default to photo
        views.dialogPhotoOrVideoPhoto.isChecked = currentMode == AppPreferences.TAKE_PHOTO_VIDEO_MODE_PHOTO
        views.dialogPhotoOrVideoVideo.isChecked = currentMode == AppPreferences.TAKE_PHOTO_VIDEO_MODE_VIDEO

        MaterialAlertDialogBuilder(activity)
                .setTitle(R.string.option_take_photo_video)
                .setView(dialogLayout)
                .setPositiveButton(R.string.save) { _, _ ->
                    submitSettings(views)
                    listener.onUpdated()
                }
                .setNegativeButton(R.string.cancel, null)
                .show()
    }

    private fun submitSettings(views: DialogPhotoOrVideoBinding) {
        vectorPreferences.setTakePhotoVideoMode(
                when {
                    views.dialogPhotoOrVideoPhoto.isChecked -> AppPreferences.TAKE_PHOTO_VIDEO_MODE_PHOTO
                    views.dialogPhotoOrVideoVideo.isChecked -> AppPreferences.TAKE_PHOTO_VIDEO_MODE_VIDEO
                    else                                    -> AppPreferences.TAKE_PHOTO_VIDEO_MODE_ALWAYS_ASK
                }
        )
    }
}
