package io.yoosh.app.core.resources

import android.content.Context
import androidx.annotation.DimenRes
import javax.inject.Inject

class DimensionProvider @Inject constructor(private val context: Context) {

    fun getDimension(@DimenRes dimenRes: Int): Float {
        return context.resources.getDimension(dimenRes)
    }
}

