/*
 * Copyright (c) 2020 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.core.epoxy.profiles

import androidx.annotation.CallSuper
import com.airbnb.epoxy.EpoxyAttribute
import io.yoosh.app.core.epoxy.AppEpoxyModel
import io.yoosh.app.core.extensions.setTextOrHide
import io.yoosh.app.core.utils.TextUtils.shortenUserId
import io.yoosh.app.features.home.AvatarRenderer
import org.matrix.android.sdk.api.util.MatrixItem

abstract class BaseProfileMatrixItem<T : ProfileMatrixItem.Holder> : AppEpoxyModel<T>() {
    @EpoxyAttribute lateinit var avatarRenderer: AvatarRenderer
    @EpoxyAttribute lateinit var matrixItem: MatrixItem

    @CallSuper
    override fun bind(holder: T) {
        super.bind(holder)
        val bestName = matrixItem.getBestName()
        val matrixId = matrixItem.id
                .takeIf { it != bestName }
                // Special case for ThreePid fake matrix item
                .takeIf { it != "@" }
        holder.titleView.text = bestName.shortenUserId()
        holder.subtitleView.setTextOrHide(matrixId?.shortenUserId())
        avatarRenderer.render(matrixItem, holder.avatarImageView)
    }
}
