/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.core.di

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProvider
import dagger.BindsInstance
import dagger.Component
import io.yoosh.app.core.dialogs.UnrecognizedCertificateDialog
import io.yoosh.app.core.error.ErrorFormatter
import io.yoosh.app.core.preference.UserAvatarPreference
import io.yoosh.app.features.MainActivity
import io.yoosh.app.features.auth.ReAuthActivity
import io.yoosh.app.features.crypto.keysbackup.settings.KeysBackupManageActivity
import io.yoosh.app.features.crypto.keysbackup.setup.KeysBackupSetupActivity
import io.yoosh.app.features.crypto.quads.SharedSecureStorageActivity
import io.yoosh.app.features.crypto.recover.BootstrapBottomSheet
import io.yoosh.app.features.crypto.verification.VerificationBottomSheet
import io.yoosh.app.features.home.HomeActivity
import io.yoosh.app.features.home.HomeModule
import io.yoosh.app.features.home.room.detail.RoomDetailActivity
import io.yoosh.app.features.home.room.detail.readreceipts.DisplayReadReceiptsBottomSheet
import io.yoosh.app.features.home.room.detail.search.SearchActivity
import io.yoosh.app.features.home.room.detail.timeline.action.MessageActionsBottomSheet
import io.yoosh.app.features.home.room.detail.timeline.reactions.ViewReactionsBottomSheet
import io.yoosh.app.features.home.room.detail.widget.RoomWidgetsBottomSheet
import io.yoosh.app.features.home.room.filtered.FilteredRoomsActivity
import io.yoosh.app.features.home.room.list.RoomListModule
import io.yoosh.app.features.home.room.list.actions.RoomListQuickActionsBottomSheet
import io.yoosh.app.features.invite.AutoAcceptInvites
import io.yoosh.app.features.invite.InviteUsersToRoomActivity
import io.yoosh.app.features.invite.AppInviteView
import io.yoosh.app.features.login.LoginActivity
import io.yoosh.app.features.matrixto.MatrixToBottomSheet
import io.yoosh.app.features.media.BigImageViewerActivity
import io.yoosh.app.features.media.AppAttachmentViewerActivity
import io.yoosh.app.features.navigation.Navigator
import io.yoosh.app.features.permalink.PermalinkHandlerActivity
import io.yoosh.app.features.qrcode.QrCodeScannerActivity
import io.yoosh.app.features.reactions.EmojiReactionPickerActivity
import io.yoosh.app.features.reactions.widget.ReactionButton
import io.yoosh.app.features.home.room.list.RoomSpaceCreationBottomSheet
import io.yoosh.app.features.roomdirectory.createroom.CreateRoomActivity
import io.yoosh.app.features.roommemberprofile.RoomMemberProfileActivity
import io.yoosh.app.features.roommemberprofile.devices.DeviceListBottomSheet
import io.yoosh.app.features.roomprofile.RoomProfileActivity
import io.yoosh.app.features.roomprofile.settings.historyvisibility.RoomHistoryVisibilityBottomSheet
import io.yoosh.app.features.settings.AppSettingsActivity
import io.yoosh.app.features.settings.devices.DeviceVerificationInfoBottomSheet
import io.yoosh.app.features.share.IncomingShareActivity
import io.yoosh.app.features.signout.soft.SoftLogoutActivity
import io.yoosh.app.features.spaces.InviteRoomSpaceChooserBottomSheet
import io.yoosh.app.features.spaces.SpaceCreationActivity
import io.yoosh.app.features.spaces.SpaceExploreActivity
import io.yoosh.app.features.spaces.SpaceSettingsMenuBottomSheet
import io.yoosh.app.features.spaces.invite.SpaceInviteBottomSheet
import io.yoosh.app.features.spaces.manage.SpaceManageActivity
import io.yoosh.app.features.spaces.share.ShareSpaceBottomSheet
import io.yoosh.app.features.terms.ReviewTermsActivity
import io.yoosh.app.features.ui.UiStateRepository
import io.yoosh.app.features.usercode.UserCodeActivity
import io.yoosh.app.features.widgets.WidgetActivity
import io.yoosh.app.features.widgets.permissions.RoomWidgetPermissionBottomSheet

@Component(
        dependencies = [
            YooshComponent::class
        ],
        modules = [
            ViewModelModule::class,
            FragmentModule::class,
            HomeModule::class,
            RoomListModule::class,
            ScreenModule::class
        ]
)
@ScreenScope
interface ScreenComponent {

    /* ==========================================================================================
     * Shortcut to VectorComponent elements
     * ========================================================================================== */

    fun activeSessionHolder(): ActiveSessionHolder
    fun fragmentFactory(): FragmentFactory
    fun viewModelFactory(): ViewModelProvider.Factory
    fun navigator(): Navigator
    fun errorFormatter(): ErrorFormatter
    fun uiStateRepository(): UiStateRepository
    fun unrecognizedCertificateDialog(): UnrecognizedCertificateDialog
    fun autoAcceptInvites(): AutoAcceptInvites

    /* ==========================================================================================
     * Activities
     * ========================================================================================== */

    fun inject(activity: HomeActivity)
    fun inject(activity: RoomDetailActivity)
    fun inject(activity: RoomProfileActivity)
    fun inject(activity: RoomMemberProfileActivity)
    fun inject(activity: AppSettingsActivity)
    fun inject(activity: KeysBackupManageActivity)
    fun inject(activity: EmojiReactionPickerActivity)
    fun inject(activity: LoginActivity)
    fun inject(activity: MainActivity)
    fun inject(activity: KeysBackupSetupActivity)
    fun inject(activity: FilteredRoomsActivity)
    fun inject(activity: CreateRoomActivity)
    fun inject(activity: IncomingShareActivity)
    fun inject(activity: SoftLogoutActivity)
    fun inject(activity: PermalinkHandlerActivity)
    fun inject(activity: QrCodeScannerActivity)
    fun inject(activity: SharedSecureStorageActivity)
    fun inject(activity: BigImageViewerActivity)
    fun inject(activity: InviteUsersToRoomActivity)
    fun inject(activity: ReviewTermsActivity)
    fun inject(activity: WidgetActivity)
    fun inject(activity: AppAttachmentViewerActivity)
    fun inject(activity: SearchActivity)
    fun inject(activity: UserCodeActivity)
    fun inject(activity: ReAuthActivity)
    fun inject(activity: SpaceCreationActivity)
    fun inject(activity: SpaceExploreActivity)
    fun inject(activity: SpaceManageActivity)

    /* ==========================================================================================
     * BottomSheets
     * ========================================================================================== */

    fun inject(bottomSheet: MessageActionsBottomSheet)
    fun inject(bottomSheet: ViewReactionsBottomSheet)
    fun inject(bottomSheet: DisplayReadReceiptsBottomSheet)
    fun inject(bottomSheet: RoomListQuickActionsBottomSheet)
    fun inject(bottomSheet: RoomSpaceCreationBottomSheet)
    fun inject(bottomSheet: RoomHistoryVisibilityBottomSheet)
    fun inject(bottomSheet: VerificationBottomSheet)
    fun inject(bottomSheet: DeviceVerificationInfoBottomSheet)
    fun inject(bottomSheet: DeviceListBottomSheet)
    fun inject(bottomSheet: BootstrapBottomSheet)
    fun inject(bottomSheet: RoomWidgetPermissionBottomSheet)
    fun inject(bottomSheet: RoomWidgetsBottomSheet)
    fun inject(bottomSheet: MatrixToBottomSheet)
    fun inject(bottomSheet: ShareSpaceBottomSheet)
    fun inject(bottomSheet: SpaceSettingsMenuBottomSheet)
    fun inject(bottomSheet: InviteRoomSpaceChooserBottomSheet)
    fun inject(bottomSheet: SpaceInviteBottomSheet)

    /* ==========================================================================================
     * Others
     * ========================================================================================== */

    fun inject(view: AppInviteView)
    fun inject(preference: UserAvatarPreference)
    fun inject(button: ReactionButton)

    /* ==========================================================================================
     * Factory
     * ========================================================================================== */

    @Component.Factory
    interface Factory {
        fun create(yooshComponent: YooshComponent,
                   @BindsInstance context: AppCompatActivity
        ): ScreenComponent
    }
}
