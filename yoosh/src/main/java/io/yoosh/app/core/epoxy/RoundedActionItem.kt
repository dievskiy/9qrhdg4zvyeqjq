/*
 * Copyright (c) 2021 Ruslan Potekhin.
 */

package io.yoosh.app.core.epoxy

import android.graphics.drawable.GradientDrawable
import io.yoosh.app.R
import io.yoosh.app.core.extensions.exhaustive
import io.yoosh.app.core.resources.DimensionProvider

enum class ItemPosition {
    FIRST, LAST, INBETWEEN, SINGLE
}

fun resolveItemPosition(index: Int, size: Int): ItemPosition {
    return when (index) {
        0        -> if (size == 1) ItemPosition.SINGLE else ItemPosition.FIRST
        size - 1 -> ItemPosition.LAST
        else     -> ItemPosition.INBETWEEN
    }
}

/**
 * Wrapper for [AppEpoxyModel] that rounds corners for drawable based on [ItemPosition]
 */
abstract class RoundedActionItem<T : AppEpoxyHolder> : AppEpoxyModel<T>() {

    private var backgroundDrawable: GradientDrawable? = null

    private var pos: ItemPosition = ItemPosition.FIRST

    private var dimProvider: DimensionProvider? = null

    private fun renderCorners() {
        val drawable = backgroundDrawable ?: return
        drawable.mutate()
        val cornerRadiusDp = dimProvider?.getDimension(R.dimen.bubble_message_radius) ?: 36f

        when (pos) {
            ItemPosition.FIRST     ->
                drawable.cornerRadii = floatArrayOf(cornerRadiusDp, cornerRadiusDp, cornerRadiusDp, cornerRadiusDp,
                        0f, 0f, 0f, 0f)
            ItemPosition.LAST      ->
                drawable.cornerRadii = floatArrayOf(0f, 0f, 0f, 0f,
                        cornerRadiusDp, cornerRadiusDp, cornerRadiusDp, cornerRadiusDp)
            ItemPosition.SINGLE    ->
                drawable.cornerRadii = floatArrayOf(cornerRadiusDp, cornerRadiusDp, cornerRadiusDp, cornerRadiusDp,
                        cornerRadiusDp, cornerRadiusDp, cornerRadiusDp, cornerRadiusDp)
            ItemPosition.INBETWEEN -> Unit
        }.exhaustive
    }

    protected fun initRounded(backgroundDrawable: GradientDrawable?, position: ItemPosition, dim: DimensionProvider?) {
        this.backgroundDrawable = backgroundDrawable
        this.pos = position
        this.dimProvider = dim
        renderCorners()
    }
}
