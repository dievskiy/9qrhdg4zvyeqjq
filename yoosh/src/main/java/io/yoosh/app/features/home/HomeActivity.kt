/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import com.airbnb.mvrx.MvRx
import com.airbnb.mvrx.viewModel
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsConstants
import com.facebook.appevents.AppEventsLogger
import com.google.android.material.appbar.MaterialToolbar
import io.yoosh.app.AppStateHandler
import io.yoosh.app.R
import io.yoosh.app.core.di.ActiveSessionHolder
import io.yoosh.app.core.di.ScreenComponent
import io.yoosh.app.core.extensions.exhaustive
import io.yoosh.app.core.extensions.hideKeyboard
import io.yoosh.app.core.extensions.registerStartForActivityResult
import io.yoosh.app.core.extensions.replaceFragment
import io.yoosh.app.core.platform.AppBaseActivity
import io.yoosh.app.core.platform.ToolbarConfigurable
import io.yoosh.app.core.pushers.PushersManager
import io.yoosh.app.databinding.ActivityHomeBinding
import io.yoosh.app.features.analytics.AnalyticsActivity
import io.yoosh.app.features.analytics.AnalyticsEvent
import io.yoosh.app.features.navigation.Navigator
import io.yoosh.app.features.notifications.NotificationDrawerManager
import io.yoosh.app.features.permalink.PermalinkHandler
import io.yoosh.app.features.popup.DefaultAppAlert
import io.yoosh.app.features.popup.PopupAlertManager
import io.yoosh.app.features.settings.AppPreferences
import io.yoosh.app.features.settings.AppSettingsActivity
import io.yoosh.app.features.spaces.SpaceCreationActivity
import io.yoosh.app.features.spaces.SpacePreviewActivity
import io.yoosh.app.features.spaces.SpaceSettingsMenuBottomSheet
import io.yoosh.app.features.spaces.invite.SpaceInviteBottomSheet
import io.yoosh.app.features.spaces.share.ShareSpaceBottomSheet
import io.yoosh.app.features.themes.ThemeUtils
import io.yoosh.app.features.workers.signout.ServerBackupStatusViewModel
import io.yoosh.app.features.workers.signout.ServerBackupStatusViewState
import io.yoosh.app.push.fcm.FcmHelper
import io.yoosh.app.space
import kotlinx.parcelize.Parcelize
import tech.story.storyview.StoryView
import tech.story.storyview.model.ImageStory
import tech.story.storyview.model.SimpleStory
import tech.story.storyview.model.Story
import org.matrix.android.sdk.api.session.initsync.InitialSyncProgressService
import org.matrix.android.sdk.api.util.MatrixItem
import timber.log.Timber
import java.util.ArrayList
import javax.inject.Inject

@Parcelize
data class HomeActivityArgs(
        val clearNotification: Boolean,
        val accountCreation: Boolean
) : Parcelable

class HomeActivity :
        AnalyticsActivity<ActivityHomeBinding>(),
        ToolbarConfigurable,
        UnknownDeviceDetectorSharedViewModel.Factory,
        ServerBackupStatusViewModel.Factory,
        UnreadMessagesSharedViewModel.Factory,
        SpaceInviteBottomSheet.InteractionListener {

    private lateinit var sharedActionViewModel: HomeSharedActionViewModel

    private val homeActivityViewModel: HomeActivityViewModel by viewModel()
    @Inject lateinit var viewModelFactory: HomeActivityViewModel.Factory

    private val serverBackupStatusViewModel: ServerBackupStatusViewModel by viewModel()
    @Inject lateinit var serverBackupviewModelFactory: ServerBackupStatusViewModel.Factory

    @Inject lateinit var activeSessionHolder: ActiveSessionHolder
    @Inject lateinit var pushManager: PushersManager
    @Inject lateinit var notificationDrawerManager: NotificationDrawerManager
    @Inject lateinit var vectorPreferences: AppPreferences
    @Inject lateinit var popupAlertManager: PopupAlertManager
    @Inject lateinit var shortcutsHandler: ShortcutsHandler
    @Inject lateinit var unknownDeviceViewModelFactory: UnknownDeviceDetectorSharedViewModel.Factory
    @Inject lateinit var unreadMessagesSharedViewModelFactory: UnreadMessagesSharedViewModel.Factory
    @Inject lateinit var permalinkHandler: PermalinkHandler
    @Inject lateinit var avatarRenderer: AvatarRenderer
    @Inject lateinit var initSyncStepFormatter: InitSyncStepFormatter
    @Inject lateinit var appStateHandler: AppStateHandler

    private val createSpaceResultLauncher = registerStartForActivityResult { activityResult ->
        if (activityResult.resultCode == Activity.RESULT_OK) {
            val spaceId = SpaceCreationActivity.getCreatedSpaceId(activityResult.data)
            views.drawerLayout.closeDrawer(GravityCompat.START)

            val postSwitchOption: Navigator.PostSwitchSpaceAction = Navigator.PostSwitchSpaceAction.None
            // Here we want to change current space to the newly created one, and then immediately open the default room
            if (spaceId != null) {
                navigator.switchToSpace(context = this,
                        spaceId = spaceId,
                        postSwitchOption)
            }
        }
    }

    private val drawerListener = object : DrawerLayout.SimpleDrawerListener() {
        override fun onDrawerStateChanged(newState: Int) {
            hideKeyboard()
        }
    }

    override fun getBinding() = ActivityHomeBinding.inflate(layoutInflater)

    override fun injectWith(injector: ScreenComponent) {
        injector.inject(this)
    }

    override fun create(initialState: UnknownDevicesState): UnknownDeviceDetectorSharedViewModel {
        return unknownDeviceViewModelFactory.create(initialState)
    }

    override fun create(initialState: ServerBackupStatusViewState): ServerBackupStatusViewModel {
        return serverBackupviewModelFactory.create(initialState)
    }

    override fun create(initialState: UnreadMessagesState): UnreadMessagesSharedViewModel {
        return unreadMessagesSharedViewModelFactory.create(initialState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FcmHelper.ensureFcmTokenIsRetrieved(this, pushManager, vectorPreferences.areNotificationEnabledForDevice())
        sharedActionViewModel = viewModelProvider.get(HomeSharedActionViewModel::class.java)
        views.drawerLayout.addDrawerListener(drawerListener)
        if (isFirstCreation()) {
            replaceFragment(R.id.homeDetailFragmentContainer, HomeDetailFragment::class.java)
            replaceFragment(R.id.homeDrawerFragmentContainer, HomeDrawerFragment::class.java)
        }

        appStateHandler.selectedRoomGroupingObservable.subscribe {
            if (it.isDefined()) {
                val spaceId = it.toList().getOrNull(0)?.space()?.roomId ?: return@subscribe
                // add space only at init
                homeActivityViewModel.handle(HomeActivityViewActions.PushSpaceId(true, spaceId))
            }
        }.disposeOnDestroy()

        sharedActionViewModel
                .observe()
                .subscribe { sharedAction ->
                    when (sharedAction) {
                        is HomeActivitySharedAction.OpenDrawer        -> views.drawerLayout.openDrawer(GravityCompat.START)
                        is HomeActivitySharedAction.CloseDrawer       -> views.drawerLayout.closeDrawer(GravityCompat.START)
                        is HomeActivitySharedAction.OpenGroup         -> {

                            val activeSpaceId = appStateHandler.safeActiveSpaceId()
                            activeSpaceId?.let {
                                homeActivityViewModel.handle(HomeActivityViewActions.PushSpaceId(false, it))
                            }

                            views.drawerLayout.closeDrawer(GravityCompat.START)

                            // Temporary
                            // When switching from space to group or group to space, we need to reload the fragment
                            // To be removed when dropping legacy groups
                            if (sharedAction.clearFragment) {
                                replaceFragment(R.id.homeDetailFragmentContainer, HomeDetailFragment::class.java, allowStateLoss = true)
                            } else {
                                // nop
                            }
                            // we might want to delay that to avoid having the drawer animation lagging
                            // would be probably better to let the drawer do that? in the on closed callback?
                        }
                        is HomeActivitySharedAction.OpenSpacePreview  -> {
                            startActivity(SpacePreviewActivity.newIntent(this, sharedAction.spaceId))
                        }
                        is HomeActivitySharedAction.AddSpace          -> {
                            val parentId = sharedAction.spaceId
                            createSpaceResultLauncher.launch(SpaceCreationActivity.newIntent(this, parentId))
                        }
                        is HomeActivitySharedAction.ShowSpaceSettings -> {
                            // open bottom sheet
                            SpaceSettingsMenuBottomSheet
                                    .newInstance(sharedAction.spaceId, object : SpaceSettingsMenuBottomSheet.InteractionListener {
                                        override fun onShareSpaceSelected(spaceId: String) {
                                            ShareSpaceBottomSheet.show(supportFragmentManager, spaceId)
                                        }

                                        override fun onSpaceDeleted(spaceId: String) {
                                            homeActivityViewModel.handle(HomeActivityViewActions.SpaceDeleted(spaceId))
                                        }
                                    })
                                    .show(supportFragmentManager, "SPACE_SETTINGS")
                        }
                        is HomeActivitySharedAction.OpenSpaceInvite   -> {
                            SpaceInviteBottomSheet.newInstance(sharedAction.spaceId)
                                    .show(supportFragmentManager, "SPACE_INVITE")
                        }
                        is HomeActivitySharedAction.TabChanged        -> {
                            homeActivityViewModel.handle(HomeActivityViewActions.ChangeTab(sharedAction.currentTab))
                        }
                        else                                          -> Unit
                    }.exhaustive
                }
                .disposeOnDestroy()

        val args = intent.getParcelableExtra<HomeActivityArgs>(MvRx.KEY_ARG)

        if (args?.clearNotification == true) {
            notificationDrawerManager.clearAllEvents()
        }

        homeActivityViewModel.observeViewEvents {
            when (it) {
                is HomeActivityViewEvents.AskPasswordToInitCrossSigning -> handleAskPasswordToInitCrossSigning(it)
                is HomeActivityViewEvents.OnNewSession                  -> handleOnNewSession(it)
                HomeActivityViewEvents.PromptToEnableSessionPush        -> handlePromptToEnablePush()
                is HomeActivityViewEvents.OnCrossSignedInvalidated      -> handleCrossSigningInvalidated(it)
                HomeActivityViewEvents.OnSpaceStackExhausted            -> super.onBackPressed()
                is HomeActivityViewEvents.SwitchToSpace                 -> handleSpaceSwitch(it)
                HomeActivityViewEvents.ShowRootSpaces                   -> handleShowRootSpaces()
                HomeActivityViewEvents.ShowIntroductionStories          -> showStories()
            }.exhaustive
        }
        homeActivityViewModel.subscribe(this) { renderState(it) }

        shortcutsHandler.observeRoomsAndBuildShortcuts()
                .disposeOnDestroy()

        if (isFirstCreation()) {
            recordEvent(AnalyticsEvent.Basic.APP_OPEN)
            initFacebookSdk()
        }
    }

    private fun showStories() {
        val myStories: ArrayList<Story> = ArrayList<Story>()
        val story1: Story = SimpleStory(
                getString(R.string.intro_story_1_title),
                getString(R.string.intro_story_1_desc)
        )

        val story2: Story = ImageStory(
                getString(R.string.intro_story_2_image),
                getString(R.string.intro_story_2_title),
                getString(R.string.intro_story_2_desc)
        )

        val story3: Story = ImageStory(
                getString(R.string.intro_story_3_image),
                getString(R.string.intro_story_3_title),
                getString(R.string.intro_story_3_desc)
        )

        val story4: Story = ImageStory(
                getString(R.string.intro_story_4_image),
                getString(R.string.intro_story_4_title),
                getString(R.string.intro_story_4_desc)
        )

        val story5: Story = SimpleStory(
                getString(R.string.intro_story_5_title),
                getString(R.string.intro_story_5_desc)
        )

        myStories.add(story1)
        myStories.add(story2)
        myStories.add(story3)
        myStories.add(story4)
        myStories.add(story5)

        try {
            StoryView.Builder(supportFragmentManager)
                    .setStoriesList(myStories)
                    .setStoryDuration(5000)
                    .setTitleText(getString(R.string.intro_story_title))
                    .setStartingIndex(0)
                    .setBackground(ContextCompat.getDrawable(this, R.drawable.stories_background))
                    .build()
                    .show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        super.onPause()
        homeActivityViewModel.cancel()
    }

    private fun initFacebookSdk() {
        try {
            FacebookSdk.setAutoLogAppEventsEnabled(true)
            FacebookSdk.setAdvertiserIDCollectionEnabled(true)
            FacebookSdk.setAutoInitEnabled(true)
            FacebookSdk.fullyInitialize()
            val logger = AppEventsLogger.newLogger(this)
            logger.logEvent(AppEventsConstants.EVENT_NAME_ACTIVATED_APP)
        } catch (t: Throwable) {
        }
    }

    private fun handleShowRootSpaces() {
        sharedActionViewModel.post(HomeActivitySharedAction.OpenRootSpaces)
    }

    private fun handleSpaceSwitch(event: HomeActivityViewEvents.SwitchToSpace) {
        navigator.switchToSpace(this, event.spaceId, Navigator.PostSwitchSpaceAction.None)
    }

    private fun renderState(state: HomeActivityViewState) {
        when (val status = state.initialSyncProgressServiceStatus) {
            is InitialSyncProgressService.Status.Idle        -> {
                views.waitingView.root.isVisible = false
            }
            is InitialSyncProgressService.Status.Progressing -> {
                val initSyncStepStr = initSyncStepFormatter.format(status.initSyncStep)
                Timber.v("$initSyncStepStr ${status.percentProgress}")
                views.waitingView.root.setOnClickListener {
                    // block interactions
                }
                views.waitingView.waitingHorizontalProgress.apply {
                    isIndeterminate = false
                    max = 100
                    progress = status.percentProgress
                    isVisible = true
                }
                views.waitingView.waitingStatusText.apply {
                    text = initSyncStepStr
                    isVisible = true
                }
                views.waitingView.root.isVisible = true
            }
            else                                             -> {
            }
        }.exhaustive
    }

    private fun handleAskPasswordToInitCrossSigning(events: HomeActivityViewEvents.AskPasswordToInitCrossSigning) {
        // We need to ask
        promptSecurityEvent(
                events.userItem,
                R.string.upgrade_security,
                R.string.security_prompt_text
        ) {
            it.navigator.upgradeSessionSecurity(it, true)
        }
    }

    private fun handleCrossSigningInvalidated(event: HomeActivityViewEvents.OnCrossSignedInvalidated) {
        // We need to ask
        promptSecurityEvent(
                event.userItem,
                R.string.crosssigning_verify_this_session,
                R.string.confirm_your_identity
        ) {
            it.navigator.waitSessionVerification(it)
        }
    }

    private fun handleOnNewSession(event: HomeActivityViewEvents.OnNewSession) {
        // We need to ask
        promptSecurityEvent(
                event.userItem,
                R.string.crosssigning_verify_this_session,
                R.string.confirm_your_identity
        ) {
            if (event.waitForIncomingRequest) {
                it.navigator.waitSessionVerification(it)
            } else {
                it.navigator.requestSelfSessionVerification(it)
            }
        }
    }

    private fun handlePromptToEnablePush() {
        popupAlertManager.postVectorAlert(
                DefaultAppAlert(
                        uid = "enablePush",
                        title = getString(R.string.alert_push_are_disabled_title),
                        description = getString(R.string.alert_push_are_disabled_description),
                        iconId = R.drawable.ic_room_actions_notifications_mutes,
                        shouldBeDisplayedIn = {
                            it is HomeActivity
                        }
                ).apply {
                    colorInt = ThemeUtils.getColor(this@HomeActivity, R.attr.app_notice_secondary)
                    contentAction = Runnable {
                        (weakCurrentActivity?.get() as? AppBaseActivity<*>)?.let {
                            // action(it)
                            homeActivityViewModel.handle(HomeActivityViewActions.PushPromptHasBeenReviewed)
                            it.navigator.openSettings(it, AppSettingsActivity.EXTRA_DIRECT_ACCESS_NOTIFICATIONS)
                        }
                    }
                    dismissedAction = Runnable {
                        homeActivityViewModel.handle(HomeActivityViewActions.PushPromptHasBeenReviewed)
                    }
                    addButton(getString(R.string.dismiss), {
                        homeActivityViewModel.handle(HomeActivityViewActions.PushPromptHasBeenReviewed)
                    }, true)
                    addButton(getString(R.string.settings), {
                        (weakCurrentActivity?.get() as? AppBaseActivity<*>)?.let {
                            // action(it)
                            homeActivityViewModel.handle(HomeActivityViewActions.PushPromptHasBeenReviewed)
                            it.navigator.openSettings(it, AppSettingsActivity.EXTRA_DIRECT_ACCESS_NOTIFICATIONS)
                        }
                    }, true)
                }
        )
    }

    private fun promptSecurityEvent(userItem: MatrixItem.UserItem?, titleRes: Int, descRes: Int, action: ((AppBaseActivity<*>) -> Unit)) {
//        popupAlertManager.postVectorAlert(
//                VerificationVectorAlert(
//                        uid = "upgradeSecurity",
//                        title = getString(titleRes),
//                        description = getString(descRes),
//                        iconId = R.drawable.ic_shield_warning
//                ).apply {
//                    viewBinder = VerificationVectorAlert.ViewBinder(userItem, avatarRenderer)
//                    colorInt = ThemeUtils.getColor(this@HomeActivity, R.attr.colorPrimary)
//                    contentAction = Runnable {
//                        (weakCurrentActivity?.get() as? VectorBaseActivity<*>)?.let {
//                            action(it)
//                        }
//                    }
//                    dismissedAction = Runnable {}
//                }
//        )
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent?.getParcelableExtra<HomeActivityArgs>(MvRx.KEY_ARG)?.clearNotification == true) {
            notificationDrawerManager.clearAllEvents()
        }
    }

    override fun onDestroy() {
        views.drawerLayout.removeDrawerListener(drawerListener)
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()

        // Force remote backup state update to update the banner if needed
        serverBackupStatusViewModel.refreshRemoteStateIfNeeded()
    }

    override fun configure(toolbar: MaterialToolbar) {
        configureToolbar(toolbar, false)
    }

    override fun onBackPressed() {
        if (views.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            views.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            homeActivityViewModel.handle(HomeActivityViewActions.PopSpaceId)
        }
    }

    override fun spaceInviteBottomSheetOnAccept(spaceId: String) {
        navigator.switchToSpace(this, spaceId, Navigator.PostSwitchSpaceAction.None)
    }

    override fun spaceInviteBottomSheetOnDecline(spaceId: String) {
        // nop
    }

    companion object {
        fun newIntent(context: Context, clearNotification: Boolean = false, accountCreation: Boolean = false): Intent {
            val args = HomeActivityArgs(
                    clearNotification = clearNotification,
                    accountCreation = accountCreation
            )

            return Intent(context, HomeActivity::class.java)
                    .apply {
                        putExtra(MvRx.KEY_ARG, args)
                    }
        }
    }
}
