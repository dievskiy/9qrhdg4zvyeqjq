/*
 * Copyright (c) 2021 Ruslan Potekhin.
 */

package io.yoosh.app.features.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.airbnb.mvrx.withState
import io.yoosh.app.R
import io.yoosh.app.core.utils.openUrlInExternalBrowser
import io.yoosh.app.databinding.FragmentLoginContinueBinding
import io.yoosh.app.features.analytics.AnalyticsEvent
import io.yoosh.app.features.home.room.detail.timeline.action.EventSharedAction
import io.yoosh.app.features.home.room.detail.timeline.action.MessageActionsEpoxyController
import io.yoosh.app.features.home.room.detail.timeline.tools.createLinkMovementMethod
import timber.log.Timber

import javax.inject.Inject

/**
 * In this screen, the user is asked to sign in with SSO provider (Google)
 */
class LoginWithProviderFragment @Inject constructor() : AbstractSSOLoginFragment<FragmentLoginContinueBinding>() {

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentLoginContinueBinding {
        return FragmentLoginContinueBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    private fun setupViews() {
        views.loginSignupSigninSubmit.setOnClickListener { submit() }
        val listener = object : MessageActionsEpoxyController.MessageActionsEpoxyControllerListener {
            override fun didSelectMenuAction(eventAction: EventSharedAction) {
            }

            override fun onUrlClicked(url: String, title: String): Boolean {
                openUrlInExternalBrowser(requireContext(), url)
                return true
            }

            override fun onUrlLongClicked(url: String): Boolean {
                return true
            }
        }
        views.loginContinuePrivacy.movementMethod = createLinkMovementMethod(listener)
    }

    private fun setupTitle(state: LoginViewState) {
        when (state.loginMode) {
            is LoginMode.Sso -> {
                views.loginContinueInfo.text = getString(R.string.login_continue_info)
                views.loginContinueTitle.text = getString(R.string.yo_)
            }
            else             -> Unit
        }
    }

    private fun submit() = withState(loginViewModel) { state ->
        Timber.v("123123 $state ---- here we should enable analytics")
        toggleAnalytics(true)
        recordEvent(AnalyticsEvent.Basic.LOGIN_ATTEMPT)
        if (state.loginMode is LoginMode.Sso) {
            loginViewModel.getSsoUrl(
                    redirectUrl = LoginActivity.YOOSH_REDIRECT_URL,
                    deviceId = state.deviceId,
                    providerId = null
            )
                    ?.let { openInCustomTab(it) }
        } else {
            loginViewModel.handle(LoginAction.UpdateSignMode(SignMode.SignUp))
        }
    }

    override fun resetViewModel() {
        loginViewModel.handle(LoginAction.ResetSignMode)
    }

    override fun updateWithState(state: LoginViewState) {
        setupTitle(state)
    }
}
