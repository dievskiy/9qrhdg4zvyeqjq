/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.spaces

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.airbnb.mvrx.args
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import io.yoosh.app.R
import io.yoosh.app.core.di.ActiveSessionHolder
import io.yoosh.app.core.di.ScreenComponent
import io.yoosh.app.core.platform.AppBaseBottomSheetDialogFragment
import io.yoosh.app.core.resources.ColorProvider
import io.yoosh.app.databinding.BottomSheetSpaceSettingsBinding
import io.yoosh.app.features.home.AvatarRenderer
import io.yoosh.app.features.navigation.Navigator
import io.yoosh.app.features.powerlevel.PowerLevelsObservableFactory
import io.yoosh.app.features.session.coroutineScope
import io.yoosh.app.features.settings.AppPreferences
import io.yoosh.app.features.spaces.manage.ManageType
import io.yoosh.app.features.spaces.manage.SpaceManageActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.launch
import kotlinx.parcelize.Parcelize
import me.gujun.android.span.span
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.room.powerlevels.PowerLevelsHelper
import org.matrix.android.sdk.api.session.room.powerlevels.Role
import org.matrix.android.sdk.api.util.toMatrixItem
import timber.log.Timber
import javax.inject.Inject

@Parcelize
data class SpaceBottomSheetSettingsArgs(
        val spaceId: String
) : Parcelable

class SpaceSettingsMenuBottomSheet : AppBaseBottomSheetDialogFragment<BottomSheetSpaceSettingsBinding>() {

    @Inject lateinit var navigator: Navigator
    @Inject lateinit var activeSessionHolder: ActiveSessionHolder
    @Inject lateinit var avatarRenderer: AvatarRenderer
    @Inject lateinit var preferences: AppPreferences
    @Inject lateinit var colorProvider: ColorProvider

    private val spaceArgs: SpaceBottomSheetSettingsArgs by args()

    interface InteractionListener {
        fun onShareSpaceSelected(spaceId: String)
        fun onSpaceDeleted(spaceId: String)
    }

    var interactionListener: InteractionListener? = null

    override val showExpanded = true

    override fun injectWith(injector: ScreenComponent) {
        injector.inject(this)
    }

    var isLastAdmin: Boolean = false

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): BottomSheetSpaceSettingsBinding {
        return BottomSheetSpaceSettingsBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val session = activeSessionHolder.getSafeActiveSession() ?: return
        val roomSummary = session.getRoomSummary(spaceArgs.spaceId)
        roomSummary?.toMatrixItem()?.let {
            avatarRenderer.render(it, views.spaceAvatarImageView)
        }
        views.spaceNameView.text = roomSummary?.displayName

        views.spaceSettings.actionTitle.text = getString(R.string.settings)
        views.spaceSettings.actionIcon.setImageResource(R.drawable.ic_room_actions_settings)

        views.leaveSpace.actionTitle.text = getString(R.string.delete)
        views.leaveSpace.actionTitle.setTextColor(resources.getColor(R.color.palette_red))
        views.leaveSpace.actionIcon.setImageResource(R.drawable.ic_delete_unsent_messages)
        views.leaveSpace.actionIcon.setColorFilter(resources.getColor(R.color.palette_red), android.graphics.PorterDuff.Mode.MULTIPLY)

        val room = session.getRoom(spaceArgs.spaceId) ?: return

        PowerLevelsObservableFactory(room)
                .createObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { powerLevelContent ->
                    val powerLevelsHelper = PowerLevelsHelper(powerLevelContent)

                    val canChangeAvatar = powerLevelsHelper.isUserAllowedToSend(session.myUserId, true, EventType.STATE_ROOM_AVATAR)
                    val canChangeName = powerLevelsHelper.isUserAllowedToSend(session.myUserId, true, EventType.STATE_ROOM_NAME)

                    views.spaceSettings.actionBg.isVisible = canChangeAvatar || canChangeName

                    val isAdmin = powerLevelsHelper.getUserRole(session.myUserId) is Role.Admin
                    val otherAdminCount = roomSummary?.otherMemberIds
                            ?.map { powerLevelsHelper.getUserRole(it) }
                            ?.count { it is Role.Admin }
                            ?: 0
                    isLastAdmin = isAdmin && otherAdminCount == 0
                }.disposeOnDestroyView()


        views.spaceSettings.actionBg.debouncedClicks {
            startActivity(SpaceManageActivity.newIntent(requireActivity(), spaceArgs.spaceId, ManageType.Settings))
            dismiss()
        }

        views.leaveSpace.actionBg.debouncedClicks {
            val warningMessage: CharSequence =
                span(getString(R.string.space_leave_prompt_msg_only_you)) {
                    textColor = colorProvider.getColorFromAttribute(R.attr.colorError)
                }

            MaterialAlertDialogBuilder(requireContext(), R.style.ThemeOverlay_Vector_MaterialAlertDialog_Destructive)
                    .setMessage(warningMessage)
                    .setTitle(getString(R.string.space_leave_prompt_msg))
                    .setPositiveButton(R.string.delete) { _, _ ->
                        session.coroutineScope.launch {
                            try {
                                session.getRoom(spaceArgs.spaceId)?.leave(null)
                                interactionListener?.onSpaceDeleted(spaceArgs.spaceId)
                            } catch (failure: Throwable) {
                                Timber.e(failure, "Failed to leave space")
                            }
                        }
                        dismiss()
                    }
                    .setNegativeButton(R.string.cancel, null)
                    .show()

            dismiss()
        }
    }

    companion object {
        fun newInstance(spaceId: String, interactionListener: InteractionListener): SpaceSettingsMenuBottomSheet {
            return SpaceSettingsMenuBottomSheet().apply {
                this.interactionListener = interactionListener
                setArguments(SpaceBottomSheetSettingsArgs(spaceId))
            }
        }
    }
}
