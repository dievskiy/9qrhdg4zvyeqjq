/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home.room.detail.timeline.item

import android.annotation.SuppressLint
import android.text.method.MovementMethod
import android.util.Log
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.PrecomputedTextCompat
import androidx.core.view.isVisible
import androidx.core.widget.TextViewCompat
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import io.yoosh.app.R
import io.yoosh.app.core.epoxy.onClick
import io.yoosh.app.features.home.room.detail.timeline.TimelineEventController
import io.yoosh.app.features.home.room.detail.timeline.tools.findPillsAndProcess
import io.yoosh.app.features.home.room.detail.timeline.url.PreviewUrlRetriever
import io.yoosh.app.features.home.room.detail.timeline.url.PreviewUrlUiState
import io.yoosh.app.features.home.room.detail.timeline.url.PreviewUrlView
import io.yoosh.app.features.media.ImageContentRenderer

@EpoxyModelClass(layout = R.layout.item_timeline_event_base)
abstract class MessageTextItem : AbsMessageItem<MessageTextItem.Holder>() {

    @EpoxyAttribute
    var searchForPills: Boolean = false

    @EpoxyAttribute
    var message: CharSequence? = null

    @EpoxyAttribute
    var canUseTextFuture: Boolean = true

    @EpoxyAttribute
    var useBigFont: Boolean = false

    @EpoxyAttribute
    var previewUrlRetriever: PreviewUrlRetriever? = null

    @EpoxyAttribute
    var previewUrlCallback: TimelineEventController.PreviewUrlCallback? = null

    @EpoxyAttribute
    var imageContentRenderer: ImageContentRenderer? = null

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var movementMethod: MovementMethod? = null

    private val previewUrlViewUpdater = PreviewUrlViewUpdater()

    private var lastClicked: Long = 0

    @SuppressLint("LogNotTimber")
    override fun bind(holder: Holder) {
        val textPadding = attributes.dimensionProvider.getDimension(R.dimen.message_text_padding).toInt()
        if (attributes.informationData.showInformation && !attributes.informationData.sentByMe) {
            holder.textRootView.setPadding(textPadding, 0, textPadding, textPadding)
        } else {
            holder.textRootView.setPadding(textPadding, textPadding, textPadding, textPadding)
        }
        // Preview URL
        previewUrlViewUpdater.previewUrlView = holder.previewUrlView
        previewUrlViewUpdater.imageContentRenderer = imageContentRenderer
        val safePreviewUrlRetriever = previewUrlRetriever
        if (safePreviewUrlRetriever == null) {
            holder.previewUrlView.isVisible = false
        } else {
            safePreviewUrlRetriever.addListener(attributes.informationData.eventId, previewUrlViewUpdater)
        }
        holder.previewUrlView.delegate = previewUrlCallback

        if (useBigFont) {
            holder.messageView.textSize = 44F
        } else {
            holder.messageView.textSize = 14F
        }
        if (searchForPills) {
            message?.findPillsAndProcess(coroutineScope) {
                // mmm.. not sure this is so safe in regards to cell reuse
                it.bind(holder.messageView)
            }
        }
        val textFuture = if (canUseTextFuture) {
            PrecomputedTextCompat.getTextFuture(
                    message ?: "",
                    TextViewCompat.getTextMetricsParams(holder.messageView),
                    null)
        } else {
            null
        }
        super.bind(holder)
        holder.messageView.movementMethod = movementMethod

        renderSendState(holder.messageView, holder.messageView)
        holder.messageView.setOnClickListener {
            val clicked = System.currentTimeMillis()
            if (clicked - lastClicked < 1000) {
                attributes.itemDoubleClickListener?.invoke(it)
            }
            lastClicked = clicked
        }
        holder.messageView.setOnLongClickListener(attributes.itemLongClickListener)

        if (canUseTextFuture) {
            holder.messageView.setTextFuture(textFuture)
        } else {
            holder.messageView.text = message
        }
    }

    override fun unbind(holder: Holder) {
        super.unbind(holder)
        previewUrlViewUpdater.previewUrlView = null
        previewUrlViewUpdater.imageContentRenderer = null
        previewUrlRetriever?.removeListener(attributes.informationData.eventId, previewUrlViewUpdater)
    }

    override fun getViewType() = STUB_ID

    class Holder : AbsMessageItem.Holder(STUB_ID) {
        val messageView by bind<AppCompatTextView>(R.id.messageTextView)
        val previewUrlView by bind<PreviewUrlView>(R.id.messageUrlPreview)
        val textRootView by bind<LinearLayout>(R.id.textRootView)
    }

    inner class PreviewUrlViewUpdater : PreviewUrlRetriever.PreviewUrlRetrieverListener {
        var previewUrlView: PreviewUrlView? = null
        var imageContentRenderer: ImageContentRenderer? = null

        override fun onStateUpdated(state: PreviewUrlUiState) {
            val safeImageContentRenderer = imageContentRenderer
            if (safeImageContentRenderer == null) {
                previewUrlView?.isVisible = false
                return
            }
            previewUrlView?.render(state, safeImageContentRenderer)
        }
    }

    companion object {
        private const val STUB_ID = R.id.messageContentTextStub
    }
}
