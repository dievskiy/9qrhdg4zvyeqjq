/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.spaces.create

import androidx.lifecycle.viewModelScope
import com.airbnb.mvrx.ActivityViewModelContext
import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.FragmentViewModelContext
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MvRxViewModelFactory
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.Uninitialized
import com.airbnb.mvrx.ViewModelContext
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import io.yoosh.app.R
import io.yoosh.app.core.error.ErrorFormatter
import io.yoosh.app.core.extensions.exhaustive
import io.yoosh.app.core.platform.AppViewModel
import io.yoosh.app.core.resources.StringProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.MatrixPatterns
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.room.failure.CreateRoomFailure

class CreateSpaceViewModel @AssistedInject constructor(
        @Assisted val initialState: CreateSpaceState,
        private val session: Session,
        private val stringProvider: StringProvider,
        private val createSpaceViewModelTask: CreateSpaceViewModelTask,
        private val errorFormatter: ErrorFormatter
) : AppViewModel<CreateSpaceState, CreateSpaceAction, CreateSpaceEvents>(initialState) {

    init {
        setState {
            copy(
                    homeServerName = session.myUserId.substringAfter(":")
            )
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(initialState: CreateSpaceState): CreateSpaceViewModel
    }

    companion object : MvRxViewModelFactory<CreateSpaceViewModel, CreateSpaceState> {

        override fun create(viewModelContext: ViewModelContext, state: CreateSpaceState): CreateSpaceViewModel? {
            val factory = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment as? Factory
                is ActivityViewModelContext -> viewModelContext.activity as? Factory
            }
            return factory?.create(state) ?: error("You should let your activity/fragment implements Factory interface")
        }

        override fun initialState(viewModelContext: ViewModelContext): CreateSpaceState {
            return CreateSpaceState()
        }
    }

    override fun handle(action: CreateSpaceAction) {
        when (action) {
            CreateSpaceAction.CreateSpace       -> {
                handleCreateSpace()
            }
            is CreateSpaceAction.NameChanged    -> {
                setState {
                    val tentativeAlias =
                            MatrixPatterns.candidateAliasFromRoomName(action.name)
                    copy(
                            nameInlineError = null,
                            name = action.name,
                            aliasLocalPart = tentativeAlias,
                            aliasVerificationTask = Uninitialized
                    )
                }
            }
            CreateSpaceAction.OnBackPressed     -> {
                _viewEvents.post(CreateSpaceEvents.Dismiss)
            }
            is CreateSpaceAction.SetAvatar      -> {
                setState { copy(avatarUri = action.uri) }
            }
            is CreateSpaceAction.SetParentSpace -> {
                setState {
                    copy(parentSpaceId = action.spaceId)
                }
            }
        }.exhaustive
    }

    private fun handleCreateSpace() = withState { state ->
        if (state.name.isNullOrBlank()) {
            setState {
                copy(
                        nameInlineError = stringProvider.getString(R.string.create_space_error_empty_field_space_name)
                )
            }
        } else {
            handleNextFromDefaultRooms()
        }
    }

    private fun handleNextFromDefaultRooms() = withState { state ->
        val spaceName = state.name ?: return@withState
        val parentSpaceId = state.parentSpaceId

        setState {
            copy(creationResult = Loading())
        }
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val result = createSpaceViewModelTask.execute(
                        CreateSpaceTaskParams(
                                spaceName = spaceName,
                                spaceTopic = state.topic,
                                spaceAvatar = state.avatarUri,
                                isPublic = false,
                                defaultRooms = emptyList(),
                                spaceAlias = null
                        )
                )
                when (result) {
                    is CreateSpaceTaskResult.Success             -> {
                        setState {
                            copy(creationResult = Success(result.spaceId))
                        }

                        if (parentSpaceId != null)
                            session.spaceService()
                                    .getSpace(parentSpaceId) // todo replace
                                    ?.addChildren(result.spaceId, viaServers = null, order = null)

                        _viewEvents.post(
                                CreateSpaceEvents.FinishSuccess(
                                        result.spaceId,
                                        result.childIds.firstOrNull(),
                                        state.spaceTopology
                                )
                        )
                    }
                    is CreateSpaceTaskResult.PartialSuccess      -> {
                        setState {
                            copy(creationResult = Success(result.spaceId))
                        }
                        _viewEvents.post(
                                CreateSpaceEvents.FinishSuccess(
                                        result.spaceId,
                                        result.childIds.firstOrNull(),
                                        state.spaceTopology
                                )
                        )
                    }
                    is CreateSpaceTaskResult.FailedToCreateSpace -> {
                        if (result.failure is CreateRoomFailure.AliasError) {
                            setState {
                                copy(
                                        aliasVerificationTask = Fail(result.failure.aliasError),
                                        creationResult = Uninitialized
                                )
                            }
                            _viewEvents.post(CreateSpaceEvents.HideModalLoading)
                        } else {
                            setState {
                                copy(creationResult = Fail(result.failure))
                            }
                            _viewEvents.post(CreateSpaceEvents.ShowModalError(errorFormatter.toHumanReadable(result.failure)))
                        }
                    }
                }
            } catch (failure: Throwable) {
                setState {
                    copy(creationResult = Fail(failure))
                }
                _viewEvents.post(CreateSpaceEvents.ShowModalError(errorFormatter.toHumanReadable(failure)))
            }
        }
    }
}
