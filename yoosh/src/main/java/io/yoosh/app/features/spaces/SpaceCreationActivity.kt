/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.spaces

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.viewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import io.yoosh.app.R
import io.yoosh.app.core.di.ScreenComponent
import io.yoosh.app.core.extensions.toMvRxBundle
import io.yoosh.app.core.platform.SimpleFragmentActivity
import io.yoosh.app.features.spaces.create.CreateSpaceAction
import io.yoosh.app.features.spaces.create.CreateSpaceDetailsFragment
import io.yoosh.app.features.spaces.create.CreateSpaceEvents
import io.yoosh.app.features.spaces.create.CreateSpaceState
import io.yoosh.app.features.spaces.create.CreateSpaceViewModel
import io.yoosh.app.features.spaces.create.SpaceTopology
import javax.inject.Inject

class SpaceCreationActivity : SimpleFragmentActivity(), CreateSpaceViewModel.Factory {

    @Inject lateinit var viewModelFactory: CreateSpaceViewModel.Factory

    override fun injectWith(injector: ScreenComponent) {
        super.injectWith(injector)
        injector.inject(this)
    }

    val viewModel: CreateSpaceViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (isFirstCreation()) {
            viewModel.handle(CreateSpaceAction.SetParentSpace(intent.getStringExtra(PARENT_SPACE_ID)))

            navigateToFragment(CreateSpaceDetailsFragment::class.java)
        }
    }

    override fun initUiAndData() {
        super.initUiAndData()

        viewModel.subscribe(this) {
            renderState(it)
        }

        viewModel.observeViewEvents {
            when (it) {
                CreateSpaceEvents.Dismiss                     -> {
                    finish()
                }
                is CreateSpaceEvents.ShowModalError           -> {
                    hideWaitingView()
                    MaterialAlertDialogBuilder(this)
                            .setMessage(it.errorMessage)
                            .setPositiveButton(getString(R.string.ok), null)
                            .show()
                }
                is CreateSpaceEvents.FinishSuccess            -> {
                    setResult(RESULT_OK, Intent().apply {
                        putExtra(RESULT_DATA_CREATED_SPACE_ID, it.spaceId)
                        putExtra(RESULT_DATA_DEFAULT_ROOM_ID, it.defaultRoomId)
                        putExtra(RESULT_DATA_CREATED_SPACE_IS_JUST_ME, it.topology == SpaceTopology.JustMe)
                    })
                    finish()
                }
                CreateSpaceEvents.HideModalLoading            -> {
                    hideWaitingView()
                }
                is CreateSpaceEvents.ShowModalLoading         -> {
                    showWaitingView(it.message)
                }
            }
        }
    }

    private fun navigateToFragment(fragmentClass: Class<out Fragment>) {
        val frag = supportFragmentManager.findFragmentByTag(fragmentClass.name) ?: createFragment(fragmentClass, Bundle().toMvRxBundle())
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.container,
                        frag,
                        fragmentClass.name
                )
                .commit()
    }

    override fun onBackPressed() {
        viewModel.handle(CreateSpaceAction.OnBackPressed)
    }

    private fun renderState(state: CreateSpaceState) {
        val titleRes = R.string.new_space
        supportActionBar?.let {
            it.title = getString(titleRes)
        } ?: run {
            title = getString(titleRes)
        }

        if (state.creationResult is Loading) {
            showWaitingView(getString(R.string.create_spaces_loading_message))
        }
    }

    companion object {
        private const val RESULT_DATA_CREATED_SPACE_ID = "RESULT_DATA_CREATED_SPACE_ID"
        private const val RESULT_DATA_DEFAULT_ROOM_ID = "RESULT_DATA_DEFAULT_ROOM_ID"
        private const val RESULT_DATA_CREATED_SPACE_IS_JUST_ME = "RESULT_DATA_CREATED_SPACE_IS_JUST_ME"

        private const val PARENT_SPACE_ID = "PARENT_SPACE_ID"

        fun newIntent(context: Context, parentId: String?): Intent {
            return Intent(context, SpaceCreationActivity::class.java).apply {
                // putExtra(MvRx.KEY_ARG, SpaceDirectoryArgs(spaceId))
                putExtra(PARENT_SPACE_ID, parentId)
            }
        }

        fun getCreatedSpaceId(data: Intent?): String? {
            return data?.extras?.getString(RESULT_DATA_CREATED_SPACE_ID)
        }

        fun getParentSpaceId(data: Intent?): String? {
            return data?.extras?.getString(PARENT_SPACE_ID)
        }

        fun getDefaultRoomId(data: Intent?): String? {
            return data?.extras?.getString(RESULT_DATA_DEFAULT_ROOM_ID)
        }

        fun isJustMeSpace(data: Intent?): Boolean {
            return data?.extras?.getBoolean(RESULT_DATA_CREATED_SPACE_IS_JUST_ME, false) == true
        }
    }

    override fun create(initialState: CreateSpaceState): CreateSpaceViewModel = viewModelFactory.create(initialState)
}
