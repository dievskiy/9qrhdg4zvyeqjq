/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.spaces.people

import com.airbnb.epoxy.TypedEpoxyController
import io.yoosh.app.R
import io.yoosh.app.core.epoxy.dividerItem
import io.yoosh.app.core.epoxy.loadingItem
import io.yoosh.app.core.epoxy.profiles.profileMatrixItemWithPowerLevel
import io.yoosh.app.core.extensions.join
import io.yoosh.app.core.resources.ColorProvider
import io.yoosh.app.core.resources.StringProvider
import io.yoosh.app.core.ui.list.genericItem
import io.yoosh.app.core.utils.DimensionConverter
import io.yoosh.app.features.home.AvatarRenderer
import io.yoosh.app.features.roomprofile.members.RoomMemberListViewState
import io.yoosh.app.features.roomprofile.members.RoomMemberSummaryFilter
import me.gujun.android.span.span
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary
import org.matrix.android.sdk.api.util.toMatrixItem
import javax.inject.Inject

class SpacePeopleListController @Inject constructor(
        private val avatarRenderer: AvatarRenderer,
        private val colorProvider: ColorProvider,
        private val stringProvider: StringProvider,
        private val dimensionConverter: DimensionConverter,
        private val roomMemberSummaryFilter: RoomMemberSummaryFilter
) : TypedEpoxyController<RoomMemberListViewState>() {

    interface InteractionListener {
        fun onSpaceMemberClicked(roomMemberSummary: RoomMemberSummary)
        fun onInviteToSpaceSelected()
    }

    var listener: InteractionListener? = null

    init {
        setData(null)
    }

    override fun buildModels(data: RoomMemberListViewState?) {
        val host = this
        val memberSummaries = data?.roomMemberSummaries?.invoke()
        if (memberSummaries == null) {
            loadingItem { id("loading") }
            return
        }
        roomMemberSummaryFilter.filter = data.filter
        var foundCount = 0
        memberSummaries.forEach { memberEntry ->

            val filtered = memberEntry.second
                    .filter { roomMemberSummaryFilter.test(it) }
            if (filtered.isNotEmpty()) {
                dividerItem {
                    id("divider_type_${memberEntry.first.titleRes}")
                }
            }
            foundCount += filtered.size
            filtered
                    .join(
                            each = { _, roomMember ->
                                profileMatrixItemWithPowerLevel {
                                    id(roomMember.userId)
                                    matrixItem(roomMember.toMatrixItem())
                                    avatarRenderer(host.avatarRenderer)
                                }
                            },
                            between = { _, roomMemberBefore ->
                                dividerItem {
                                    id("divider_${roomMemberBefore.userId}")
                                }
                            }
                    )
        }

        if (foundCount == 0 && data.filter.isNotEmpty()) {
            // add the footer thing
            genericItem {
                id("not_found")
                title(
                        span {
                            +"\n"
                            +host.stringProvider.getString(R.string.no_result_placeholder)
                        }
                )
                description(
                        span {
                            +host.stringProvider.getString(R.string.looking_for_someone_not_in_space, data.roomSummary.invoke()?.displayName ?: "")
                            +"\n"
                            span("Invite them") {
                                textColor = host.colorProvider.getColorFromAttribute(R.attr.colorPrimary)
                                textStyle = "bold"
                            }
                        }
                )
                itemClickAction {
                    host.listener?.onInviteToSpaceSelected()
                }
            }
        }
    }

}
