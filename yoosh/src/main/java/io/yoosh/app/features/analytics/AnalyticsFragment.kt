package io.yoosh.app.features.analytics

import android.os.Bundle
import android.view.View
import androidx.viewbinding.ViewBinding
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import io.yoosh.app.core.platform.AppBaseFragment

abstract class AnalyticsFragment<VB : ViewBinding> : AppBaseFragment<VB>(), AnalyticsRecorder {

    private var firebaseAnalytics: FirebaseAnalytics? = null
    private var eventRecorder: AnalyticsRecorder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = (this.activity as? FirebaseAnalyticsActivity)?.getAnalytics()
        firebaseAnalytics?.let {
            eventRecorder = EventRecorder(it)
        }
    }

    override fun recordViewTouched(view: View) {
        eventRecorder?.recordViewTouched(view)
    }

    override fun recordEvent(event: AnalyticsEvent.Basic) {
        eventRecorder?.recordEvent(event)
    }

    override fun toggleAnalytics(enabled: Boolean) {
        eventRecorder?.toggleAnalytics(enabled)
    }

    override fun recordCustomEvent(name: String, values: Map<String, String>?) {
        eventRecorder?.recordCustomEvent(name, values)
    }
}
