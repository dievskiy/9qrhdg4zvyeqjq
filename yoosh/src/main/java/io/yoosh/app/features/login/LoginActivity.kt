/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.core.view.isVisible
import com.airbnb.mvrx.viewModel
import com.google.android.material.appbar.MaterialToolbar
import io.yoosh.app.R
import io.yoosh.app.core.di.ScreenComponent
import io.yoosh.app.core.extensions.addFragment
import io.yoosh.app.core.platform.ToolbarConfigurable
import io.yoosh.app.core.platform.AppBaseActivity
import io.yoosh.app.databinding.ActivityLoginBinding
import io.yoosh.app.features.analytics.AnalyticsActivity
import io.yoosh.app.features.analytics.AnalyticsEvent
import io.yoosh.app.features.home.HomeActivity
import org.matrix.android.sdk.api.extensions.tryOrNull
import javax.inject.Inject

open class LoginActivity : AnalyticsActivity<ActivityLoginBinding>(), ToolbarConfigurable {

    private val loginViewModel: LoginViewModel by viewModel()

    @Inject lateinit var loginViewModelFactory: LoginViewModel.Factory

    override val analyticsEnabled = false

    @CallSuper
    override fun injectWith(injector: ScreenComponent) {
        injector.inject(this)
    }

    final override fun getBinding() = ActivityLoginBinding.inflate(layoutInflater)

    override fun getCoordinatorLayout() = views.coordinatorLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loginViewModel.handle(LoginAction.UpdateHomeServer(HOME_SERVER_URL))
    }

    override fun initUiAndData() {
        if (isFirstCreation()) {
            addFirstFragment()
        }

        loginViewModel
                .subscribe(this) {
                    updateWithState(it)
                }

        // Get config extra
        val loginConfig = intent.getParcelableExtra<LoginConfig?>(EXTRA_CONFIG)
        if (isFirstCreation()) {
            // TODO Check this
            loginViewModel.handle(LoginAction.InitWith(loginConfig))
        }
    }

    protected open fun addFirstFragment() {
        // no-op
        addFragment(R.id.loginFragmentContainer, LoginWithProviderFragment::class.java)
    }

    private fun updateWithState(loginViewState: LoginViewState) {
        if (loginViewState.isUserLogged()) {
            recordEvent(AnalyticsEvent.Basic.LOGIN)
            val intent = HomeActivity.newIntent(
                    this,
                    accountCreation = loginViewState.signMode == SignMode.SignUp
            )
            startActivity(intent)
            finish()
            return
        }

        // Loading
        views.loginLoading.isVisible = loginViewState.isLoading()
    }

    /**
     * Handle the SSO redirection here
     */
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        intent?.data
                ?.let { tryOrNull { it.getQueryParameter("loginToken") } }
                ?.let { loginViewModel.handle(LoginAction.LoginWithToken(it)) }
    }

    override fun configure(toolbar: MaterialToolbar) {
        configureToolbar(toolbar)
    }

    companion object {
        private const val EXTRA_CONFIG = "EXTRA_CONFIG"
        private const val HOME_SERVER_URL: String = "https://api.yoosh.io"

        // Note that the domain can be displayed to the user for confirmation that he trusts it. So use a human readable string
        const val YOOSH_REDIRECT_URL = "yoosh://connect"

        fun newIntent(context: Context, loginConfig: LoginConfig?): Intent {
            return Intent(context, LoginActivity::class.java).apply {
                putExtra(EXTRA_CONFIG, loginConfig)
            }
        }
    }
}
