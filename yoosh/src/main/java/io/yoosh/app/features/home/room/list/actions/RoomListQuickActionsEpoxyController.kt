/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.yoosh.app.features.home.room.list.actions

import com.airbnb.epoxy.TypedEpoxyController
import io.yoosh.app.core.epoxy.ItemPosition
import io.yoosh.app.core.epoxy.bottomsheet.BottomSheetRoomPreviewItem
import io.yoosh.app.core.epoxy.bottomsheet.bottomSheetActionItem
import io.yoosh.app.core.epoxy.bottomsheet.bottomSheetRoomPreviewItem
import io.yoosh.app.core.epoxy.dividerItem
import io.yoosh.app.core.resources.ColorProvider
import io.yoosh.app.core.resources.DimensionProvider
import io.yoosh.app.core.resources.StringProvider
import io.yoosh.app.features.home.AvatarRenderer
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import org.matrix.android.sdk.api.util.toMatrixItem
import javax.inject.Inject

/**
 * Epoxy controller for room list actions
 */
class RoomListQuickActionsEpoxyController @Inject constructor(
        private val avatarRenderer: AvatarRenderer,
        private val colorProvider: ColorProvider,
        private val stringProvider: StringProvider,
        private val dimensionProvider: DimensionProvider
) : TypedEpoxyController<RoomListQuickActionsState>() {

    var listener: Listener? = null

    override fun buildModels(state: RoomListQuickActionsState) {
        val roomSummary = state.roomSummary() ?: return
        val host = this
        val showAll = state.mode == RoomListActionsArgs.Mode.FULL

        if (showAll) {
            // Preview, favorite, settings
            bottomSheetRoomPreviewItem {
                id("room_preview")
                avatarRenderer(host.avatarRenderer)
                matrixItem(roomSummary.toMatrixItem())
                stringProvider(host.stringProvider)
                colorProvider(host.colorProvider)
                notificationState(state.roomNotificationState() ?: RoomNotificationState.ALL_MESSAGES_NOISY)
                izFavorite(roomSummary.isFavorite)
                favoriteClickListener { host.listener?.didSelectMenuAction(RoomListQuickActionsSharedAction.Favorite(roomSummary.roomId)) }
                notificationClickListener(object : BottomSheetRoomPreviewItem.NotificationClickListener {
                    override fun onClicked(enabled: Boolean) {
                        if (enabled) host.listener?.didSelectMenuAction(RoomListQuickActionsSharedAction.NotificationsAllNoisy(roomSummary.roomId))
                        else host.listener?.didSelectMenuAction(RoomListQuickActionsSharedAction.NotificationsMute(roomSummary.roomId))
                    }
                })
            }
        }

        RoomListQuickActionsSharedAction.Settings(roomSummary.roomId).toBottomSheetItem(0, position = ItemPosition.FIRST)
        if (showAll) {
            RoomListQuickActionsSharedAction.Leave(roomSummary.roomId).toBottomSheetItem(2, position = ItemPosition.LAST)
        }
    }

    private fun RoomListQuickActionsSharedAction.toBottomSheetItem(index: Int, roomNotificationState: RoomNotificationState? = null, position: ItemPosition) {
        val host = this@RoomListQuickActionsEpoxyController
        bottomSheetActionItem {
            id("action_$index")
            iconRes(iconResId)
            textRes(titleRes)
            position(position)
            destructive(this@toBottomSheetItem.destructive)
            dimensionProvider(host.dimensionProvider)
            listener { host.listener?.didSelectMenuAction(this@toBottomSheetItem) }
        }
        if (position != ItemPosition.LAST)
            dividerItem {
                id("divider_action_${index}")
            }
    }

    interface Listener {
        fun didSelectMenuAction(quickAction: RoomListQuickActionsSharedAction)
    }
}
