/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.settings

import androidx.preference.Preference
import io.yoosh.app.R
import io.yoosh.app.core.preference.AppPreference
import io.yoosh.app.core.utils.FirstThrottler
import io.yoosh.app.core.utils.copyToClipboard
import io.yoosh.app.core.utils.displayInWebView
import io.yoosh.app.core.utils.openUrlInChromeCustomTab
import io.yoosh.app.features.version.VersionProvider
import io.yoosh.app.openOssLicensesMenuActivity
import javax.inject.Inject

class AppSettingsHelpAboutFragment @Inject constructor(
        private val versionProvider: VersionProvider
) : AppSettingsBaseFragment() {

    override var titleRes = R.string.preference_root_help_about
    override val preferenceXmlRes = R.xml.app_settings_help_about

    private val firstThrottler = FirstThrottler(1000)

    override fun bindPref() {
        // application version
        findPreference<AppPreference>(AppPreferences.SETTINGS_VERSION_PREFERENCE_KEY)!!.let {
            it.summary = buildString {
                append(versionProvider.getVersion())
            }

            it.setOnPreferenceClickListener { pref ->
                copyToClipboard(requireContext(), pref.summary)
                true
            }
        }

        // privacy policy
        findPreference<AppPreference>(AppPreferences.SETTINGS_PRIVACY_POLICY_PREFERENCE_KEY)!!
                .onPreferenceClickListener = Preference.OnPreferenceClickListener {
            openUrlInChromeCustomTab(requireContext(), null, AppSettingsUrls.PRIVACY_POLICY)
            false
        }

        // third party notice
        findPreference<AppPreference>(AppPreferences.SETTINGS_THIRD_PARTY_NOTICES_PREFERENCE_KEY)!!
                .onPreferenceClickListener = Preference.OnPreferenceClickListener {
            if (firstThrottler.canHandle() is FirstThrottler.CanHandlerResult.Yes) {
                activity?.displayInWebView(AppSettingsUrls.THIRD_PARTY_LICENSES)
            }
            false
        }

        // Note: preference is not visible on F-Droid build
        findPreference<AppPreference>(AppPreferences.SETTINGS_OTHER_THIRD_PARTY_NOTICES_PREFERENCE_KEY)!!
                .onPreferenceClickListener = Preference.OnPreferenceClickListener {
            // See https://developers.google.com/android/guides/opensource
            openOssLicensesMenuActivity(requireActivity())
            false
        }
    }

}
