/*
 * Copyright 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home.room.detail.timeline.factory

import io.yoosh.app.ActiveSessionDataSource
import io.yoosh.app.core.epoxy.AppEpoxyModel
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.widgets.model.WidgetContent
import org.matrix.android.sdk.api.session.widgets.model.WidgetType
import javax.inject.Inject

class WidgetItemFactory @Inject constructor(
        private val noticeItemFactory: NoticeItemFactory,
        private val activeSessionDataSource: ActiveSessionDataSource
) {
    private val currentUserId: String?
        get() = activeSessionDataSource.currentValue?.orNull()?.myUserId

    fun create(params: TimelineItemFactoryParams): AppEpoxyModel<*>? {
        val event = params.event
        val widgetContent: WidgetContent = event.root.getClearContent().toModel() ?: return null
        val previousWidgetContent: WidgetContent? = event.root.resolvedPrevContent().toModel()

        return when (WidgetType.fromString(widgetContent.type ?: previousWidgetContent?.type ?: "")) {
            else             -> noticeItemFactory.create(params)
        }
    }

}
