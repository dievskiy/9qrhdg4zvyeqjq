/*
 * Copyright (c) 2021 Ruslan Potekhin.
 */

package io.yoosh.app.features.home.room.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import io.yoosh.app.core.di.ScreenComponent
import io.yoosh.app.core.platform.AppBaseBottomSheetDialogFragment
import io.yoosh.app.databinding.BottomSheetCreateRoomSpaceBinding

class RoomSpaceCreationBottomSheet : AppBaseBottomSheetDialogFragment<BottomSheetCreateRoomSpaceBinding>() {

    override val showExpanded = true

    var listener: Listener? = null

    override fun injectWith(injector: ScreenComponent) {
        injector.inject(this)
    }

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): BottomSheetCreateRoomSpaceBinding {
        return BottomSheetCreateRoomSpaceBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        views.bottomSheetCreateSpace.debouncedClicks {
            dismiss()
            listener?.openCreateSpace()
        }

        views.bottomSheetCreateChannel.debouncedClicks {
            dismiss()
            listener?.openCreateChannel()
        }
    }

    companion object {
        fun show(fragmentManager: FragmentManager): RoomSpaceCreationBottomSheet {
            return RoomSpaceCreationBottomSheet().also {
                it.show(fragmentManager, RoomSpaceCreationBottomSheet::class.java.name)
            }
        }
    }

    interface Listener {
        fun openCreateSpace()
        fun openCreateChannel()
    }
}
