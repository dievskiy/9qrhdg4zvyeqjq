/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import com.airbnb.mvrx.activityViewModel
import com.airbnb.mvrx.fragmentViewModel
import com.airbnb.mvrx.withState
import io.yoosh.app.R
import io.yoosh.app.RoomGroupingMethod
import io.yoosh.app.core.extensions.addFragment
import io.yoosh.app.core.extensions.commitTransaction
import io.yoosh.app.core.extensions.exhaustive
import io.yoosh.app.core.extensions.toMvRxBundle
import io.yoosh.app.core.platform.ToolbarConfigurable
import io.yoosh.app.core.platform.AppBaseActivity
import io.yoosh.app.core.platform.AppBaseFragment
import io.yoosh.app.core.resources.ColorProvider
import io.yoosh.app.databinding.FragmentHomeDetailBinding
import io.yoosh.app.features.analytics.AnalyticsActivity
import io.yoosh.app.features.analytics.AnalyticsEvent
import io.yoosh.app.features.analytics.AnalyticsFragment
import io.yoosh.app.features.home.room.list.RoomListFragment
import io.yoosh.app.features.home.room.list.RoomListParams
import io.yoosh.app.features.home.room.list.RoomSpaceCreationBottomSheet
import io.yoosh.app.features.popup.PopupAlertManager
import io.yoosh.app.features.popup.VerificationAppAlert
import io.yoosh.app.features.settings.AppSettingsActivity.Companion.EXTRA_DIRECT_ACCESS_SECURITY_PRIVACY_MANAGE_SESSIONS
import io.yoosh.app.features.spaces.SpaceHomeListFragment
import io.yoosh.app.features.workers.signout.ServerBackupStatusViewModel
import io.yoosh.app.features.workers.signout.ServerBackupStatusViewState
import io.yoosh.app.space
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.util.toMatrixItem
import org.matrix.android.sdk.internal.crypto.model.rest.DeviceInfo
import javax.inject.Inject

class HomeDetailFragment @Inject constructor(
        val homeDetailViewModelFactory: HomeDetailViewModel.Factory,
        private val serverBackupStatusViewModelFactory: ServerBackupStatusViewModel.Factory,
        private val avatarRenderer: AvatarRenderer,
        private val colorProvider: ColorProvider,
        private val alertManager: PopupAlertManager,
) : AnalyticsFragment<FragmentHomeDetailBinding>(),
        ServerBackupStatusViewModel.Factory {

    private val viewModel: HomeDetailViewModel by fragmentViewModel()
    private val unknownDeviceDetectorSharedViewModel: UnknownDeviceDetectorSharedViewModel by activityViewModel()
//    private val unreadMessagesSharedViewModel: UnreadMessagesSharedViewModel by activityViewModel()

    private lateinit var sharedActionViewModel: HomeSharedActionViewModel

    private var favsChanged = true

    private var hasUnreadRooms = false
        set(value) {
            if (value != field) {
                field = value
                invalidateOptionsMenu()
            }
        }

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentHomeDetailBinding {
        return FragmentHomeDetailBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState == null) {
            addFragment(R.id.spacesListContainer, SpaceHomeListFragment::class.java)
        }

        sharedActionViewModel = activityViewModelProvider.get(HomeSharedActionViewModel::class.java)

        sharedActionViewModel
                .observe()
                .subscribe { handleQuickActions(it) }
                .disposeOnDestroyView()

        setupBottomNavigationView()
        setupToolbar()
        setupSearchBar()
        setupCreateRoomButton()

        withState(viewModel) {
            // Update the navigation view if needed (for when we restore the tabs)
            views.bottomNavigationView.selectedItemId = it.currentTab.toMenuId()
        }

        viewModel.selectSubscribe(this, HomeDetailViewState::currentTab) { currentTab ->
            sharedActionViewModel.post(HomeActivitySharedAction.TabChanged(currentTab))
        }

        viewModel.observeViewEvents { viewEvent ->
            when (viewEvent) {
                HomeDetailViewEvents.Loading -> showLoadingDialog()
                else                         -> Unit
            }
        }

        unknownDeviceDetectorSharedViewModel.subscribe { state ->
            state.unknownSessions.invoke()?.let { unknownDevices ->
                if (unknownDevices.firstOrNull()?.currentSessionTrust == true) {
                    val uid = "review_login"
                    alertManager.cancelAlert(uid)
                    val olderUnverified = unknownDevices.filter { !it.isNew }
                    val newest = unknownDevices.firstOrNull { it.isNew }?.deviceInfo
                    if (newest != null) {
                        promptForNewUnknownDevices(uid, state, newest)
                    } else if (olderUnverified.isNotEmpty()) {
                        // In this case we prompt to go to settings to review logins
                        promptToReviewChanges(uid, state, olderUnverified.map { it.deviceInfo })
                    }
                }
            }
        }

//        unreadMessagesSharedViewModel.subscribe { state ->
//            views.drawerUnreadCounterBadgeView.render(
//                    UnreadCounterBadgeView.State(
//                            count = state.otherSpacesUnread.totalCount,
//                            highlighted = state.otherSpacesUnread.isHighlight
//                    )
//            )
//        }
    }

    private fun handleQuickActions(action: HomeActivitySharedAction?) {
        when (action) {
            is HomeActivitySharedAction.FavsChanged -> {
                withState(viewModel) {
                    if (it.currentTab.toMenuId() != R.id.bottom_action_favs) favsChanged = true
                }
            }
            else                                    -> Unit
        }
    }

    private fun setupSearchBar() {
        views.filteredRoomsSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                views.filteredRoomsSearchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                val fragment = childFragmentManager.findFragmentByTag("FRAGMENT_TAG_RoomList(displayMode=FILTERED)") as? RoomListFragment
                fragment?.filterRoomsWith(newText)
                return true
            }
        })
    }

    private fun setupCreateRoomButton() {
        views.createGroupRoomButton.debouncedClicks {
            recordViewTouched(views.createGroupRoomButton)
            RoomSpaceCreationBottomSheet.show(childFragmentManager)?.apply {
                listener = object : RoomSpaceCreationBottomSheet.Listener {
                    override fun openCreateSpace() {
                        recordEvent(AnalyticsEvent.Basic.OPEN_CREATE_SPACE)
                        val spaceId = withState(viewModel) { state -> state.roomGroupingMethod.space()?.roomId }
                        sharedActionViewModel.post(HomeActivitySharedAction.AddSpace(spaceId))
                    }

                    override fun openCreateChannel() {
                        recordEvent(AnalyticsEvent.Basic.OPEN_CREATE_CHANNEL)
                        val spaceId = withState(viewModel) { state -> state.roomGroupingMethod.space()?.roomId }
                        navigator.openCreateRoom(requireContext(), "", spaceId)
                    }
                }
            }
        }
    }

    private fun promptForNewUnknownDevices(uid: String, state: UnknownDevicesState, newest: DeviceInfo) {
        val user = state.myMatrixItem
        alertManager.postVectorAlert(
                VerificationAppAlert(
                        uid = uid,
                        title = getString(R.string.new_session),
                        description = getString(R.string.verify_this_session, newest.displayName ?: newest.deviceId ?: ""),
                        iconId = R.drawable.ic_shield_warning
                ).apply {
                    viewBinder = VerificationAppAlert.ViewBinder(user, avatarRenderer)
                    colorInt = colorProvider.getColorFromAttribute(R.attr.colorPrimary)
                    contentAction = Runnable {
                        (weakCurrentActivity?.get() as? AppBaseActivity<*>)
                                ?.navigator
                                ?.requestSessionVerification(requireContext(), newest.deviceId ?: "")
                        unknownDeviceDetectorSharedViewModel.handle(
                                UnknownDeviceDetectorSharedViewModel.Action.IgnoreDevice(newest.deviceId?.let { listOf(it) }.orEmpty())
                        )
                    }
                    dismissedAction = Runnable {
                        unknownDeviceDetectorSharedViewModel.handle(
                                UnknownDeviceDetectorSharedViewModel.Action.IgnoreDevice(newest.deviceId?.let { listOf(it) }.orEmpty())
                        )
                    }
                }
        )
    }

    private fun promptToReviewChanges(uid: String, state: UnknownDevicesState, oldUnverified: List<DeviceInfo>) {
        val user = state.myMatrixItem
        alertManager.postVectorAlert(
                VerificationAppAlert(
                        uid = uid,
                        title = getString(R.string.review_logins),
                        description = getString(R.string.verify_other_sessions),
                        iconId = R.drawable.ic_shield_warning
                ).apply {
                    viewBinder = VerificationAppAlert.ViewBinder(user, avatarRenderer)
                    colorInt = colorProvider.getColorFromAttribute(R.attr.colorPrimary)
                    contentAction = Runnable {
                        (weakCurrentActivity?.get() as? AppBaseActivity<*>)?.let {
                            // mark as ignored to avoid showing it again
                            unknownDeviceDetectorSharedViewModel.handle(
                                    UnknownDeviceDetectorSharedViewModel.Action.IgnoreDevice(oldUnverified.mapNotNull { it.deviceId })
                            )
                            it.navigator.openSettings(it, EXTRA_DIRECT_ACCESS_SECURITY_PRIVACY_MANAGE_SESSIONS)
                        }
                    }
                    dismissedAction = Runnable {
                        unknownDeviceDetectorSharedViewModel.handle(
                                UnknownDeviceDetectorSharedViewModel.Action.IgnoreDevice(oldUnverified.mapNotNull { it.deviceId })
                        )
                    }
                }
        )
    }

    private fun setupToolbar() {
        val parentActivity = appBaseActivity
        if (parentActivity is ToolbarConfigurable) {
            parentActivity.configure(views.groupToolbar)
        }
        views.groupToolbar.title = ""
        views.groupToolbarAvatarImageView.debouncedClicks {
            withState(viewModel) {
                val atRoot = it.roomGroupingMethod is RoomGroupingMethod.BySpace && it.roomGroupingMethod.spaceSummary == null
                if (atRoot || it.currentTab.toMenuId() != R.id.bottom_action_rooms) {
                    sharedActionViewModel.post(HomeActivitySharedAction.OpenDrawer)
                } else {
                    activity?.onBackPressed()
                }
            }
        }

        views.homeToolbarContent.debouncedClicks {
            withState(viewModel) {
                when (it.roomGroupingMethod) {
                    is RoomGroupingMethod.ByLegacyGroup -> {
                        // nothing do far
                    }
                    is RoomGroupingMethod.BySpace       -> {
                        it.roomGroupingMethod.spaceSummary?.let {
                            sharedActionViewModel.post(HomeActivitySharedAction.ShowSpaceSettings(it.roomId))
                        }
                    }
                }
            }
        }
    }

    private fun setupBottomNavigationView() {
        views.bottomNavigationView.setOnNavigationItemSelectedListener {
            val tab = when (it.itemId) {
                R.id.bottom_action_favs   -> HomeTab.RoomList(RoomListDisplayMode.FAVS)
                R.id.bottom_action_rooms  -> HomeTab.RoomList(RoomListDisplayMode.ROOMS)
                R.id.bottom_action_shared -> HomeTab.RoomList(RoomListDisplayMode.SHARED)
                else                      -> HomeTab.RoomList(RoomListDisplayMode.FILTERED)
            }
            viewModel.handle(HomeDetailAction.SwitchTab(tab))
            true
        }

//        val menuView = bottomNavigationView.getChildAt(0) as BottomNavigationMenuView

//        bottomNavigationView.getOrCreateBadge()
//        menuView.forEachIndexed { index, view ->
//            val itemView = view as BottomNavigationItemView
//            val badgeLayout = LayoutInflater.from(requireContext()).inflate(R.layout.vector_home_badge_unread_layout, menuView, false)
//            val unreadCounterBadgeView: UnreadCounterBadgeView = badgeLayout.findViewById(R.id.actionUnreadCounterBadgeView)
//            itemView.addView(badgeLayout)
//            unreadCounterBadgeViews.add(index, unreadCounterBadgeView)
//        }
    }

    private fun getCurrentRoomSummary(): RoomSummary? {
        return withState(viewModel) {
            when (it.roomGroupingMethod) {
                is RoomGroupingMethod.ByLegacyGroup -> null
                is RoomGroupingMethod.BySpace       -> it.roomGroupingMethod.spaceSummary
            }.exhaustive
        }
    }

    private fun updateToolbar(tab: HomeTab) {
        val spaceSummary = getCurrentRoomSummary()
        val title = when (tab.toMenuId()) {
            R.id.bottom_action_shared -> getString(R.string.bottom_action_shared)
            R.id.bottom_action_favs   -> getString(R.string.bottom_action_favs)
            else                      -> spaceSummary?.displayName ?: getString(R.string.title_hub)
        }
        views.groupToolbarTitleView.text = title

        val searchBarVisible = withState(viewModel) {
            it.currentTab.toMenuId() == R.id.bottom_action_search
        }

        views.filteredRoomsSearchView.isVisible = searchBarVisible
        views.homeToolbarContent.isVisible = !searchBarVisible

        if (tab.toMenuId() == R.id.bottom_action_rooms) {
            spaceSummary?.toMatrixItem()?.let {
                avatarRenderer.render(it, views.detailToolbarAvatarImageView)
                views.detailToolbarAvatarImageView.isVisible = true
                views.homeToolbarActionIcon.setBackgroundResource(R.drawable.ic_back_24dp)
            } ?: run {
                views.detailToolbarAvatarImageView.isVisible = false
                views.homeToolbarActionIcon.setBackgroundResource(R.drawable.ic_space_icons)
            }
        } else {
            views.detailToolbarAvatarImageView.isVisible = false
            views.homeToolbarActionIcon.setBackgroundResource(R.drawable.ic_space_icons)
        }
    }

    private fun updateUIForTab(tab: HomeTab) {
        updateSelectedFragment(tab)
        updateSearchView(tab)
        invalidateOptionsMenu()
        views.createGroupRoomButton.isVisible = tab.toMenuId() == R.id.bottom_action_rooms
        updateToolbar(tab)
    }

    private fun updateSearchView(tab: HomeTab) {
        val searchTabVisible = when (tab.toMenuId()) {
            R.id.bottom_action_search -> true
            else                      -> false
        }
        views.filteredRoomsSearchView.isVisible = searchTabVisible
    }

    private fun updateSelectedFragment(tab: HomeTab) {
        val fragmentTag = "FRAGMENT_TAG_$tab"
        val fragmentToShow = childFragmentManager.findFragmentByTag(fragmentTag)
        childFragmentManager.commitTransaction {
            childFragmentManager.fragments
                    .filter { it != fragmentToShow }
                    .forEach {
                        detach(it)
                    }
            if (fragmentToShow == null || (fragmentTag.contains("displayMode=FAVS") && favsChanged)) {
                if (fragmentTag.contains("displayMode=FAVS")) {
                    favsChanged = false
                    if (fragmentToShow != null) {
                        remove(fragmentToShow)
                    }
                }
                when (tab) {
                    is HomeTab.RoomList -> {
                        val params = RoomListParams(tab.displayMode)
                        add(R.id.roomListContainer, RoomListFragment::class.java, params.toMvRxBundle(), fragmentTag)
                    }
                }
            } else {
                attach(fragmentToShow)
            }
        }
    }

    private fun updateSpacesVisibility() {
        val wasVisible = views.spacesListContainer.isVisible
        val shouldBeVisible: Boolean = withState(viewModel) {
            it.currentTab.toMenuId() == R.id.bottom_action_rooms
        }

        if (wasVisible && !shouldBeVisible) {
            views.spacesListContainer.isVisible = false
        } else if (!wasVisible && shouldBeVisible) {
            views.spacesListContainer.isVisible = true
        }
    }

    override fun invalidate() = withState(viewModel) {
        hasUnreadRooms = it.hasUnreadMessages
        updateSpacesVisibility()
        updateUIForTab(it.currentTab)
    }

    private fun HomeTab.toMenuId() = when (this) {
        is HomeTab.RoomList -> when (displayMode) {
            RoomListDisplayMode.FAVS   -> R.id.bottom_action_favs
            RoomListDisplayMode.ROOMS  -> R.id.bottom_action_rooms
            RoomListDisplayMode.SHARED -> R.id.bottom_action_shared
            else                       -> R.id.bottom_action_search
        }
    }

    override fun create(initialState: ServerBackupStatusViewState): ServerBackupStatusViewModel {
        return serverBackupStatusViewModelFactory.create(initialState)
    }
}
