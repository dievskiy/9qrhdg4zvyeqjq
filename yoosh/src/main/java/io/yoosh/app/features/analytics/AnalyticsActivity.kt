package io.yoosh.app.features.analytics

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.viewbinding.ViewBinding
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import io.yoosh.app.core.platform.AppBaseActivity
import timber.log.Timber

abstract class AnalyticsActivity<VB : ViewBinding> : AppBaseActivity<VB>(), FirebaseAnalyticsActivity, AnalyticsRecorder {

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    protected open val analyticsEnabled: Boolean = true
    private var eventRecorder: AnalyticsRecorder? = null

    override fun getAnalytics(): FirebaseAnalytics? =
            if (this::firebaseAnalytics.isInitialized) {
                firebaseAnalytics
            } else null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            firebaseAnalytics = Firebase.analytics
            firebaseAnalytics.setAnalyticsCollectionEnabled(analyticsEnabled)
            recordScreenView()
        } catch (e: Throwable) {
            Timber.e(e);
        }
    }

    private fun recordScreenView() {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_CLASS, javaClass.kotlin.qualifiedName ?: "")
        }
    }

    override fun recordViewTouched(view: View) {
        eventRecorder?.recordViewTouched(view)
    }

    override fun recordEvent(event: AnalyticsEvent.Basic) {
        eventRecorder?.recordEvent(event)
    }

    override fun toggleAnalytics(enabled: Boolean) {
        eventRecorder?.toggleAnalytics(enabled)
    }

    override fun recordCustomEvent(name: String, values: Map<String, String>?) {
        eventRecorder?.recordCustomEvent(name, values)
    }
}

interface FirebaseAnalyticsActivity {
    fun getAnalytics(): FirebaseAnalytics?
}
