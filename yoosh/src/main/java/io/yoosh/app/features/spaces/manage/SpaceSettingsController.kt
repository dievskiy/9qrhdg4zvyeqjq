/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.spaces.manage

import com.airbnb.epoxy.TypedEpoxyController
import io.yoosh.app.R
import io.yoosh.app.core.epoxy.dividerItem
import io.yoosh.app.core.epoxy.profiles.buildProfileAction
import io.yoosh.app.core.epoxy.profiles.buildProfileSection
import io.yoosh.app.core.resources.DimensionProvider
import io.yoosh.app.core.resources.StringProvider
import io.yoosh.app.core.ui.list.verticalMarginItem
import io.yoosh.app.features.form.formEditTextItem
import io.yoosh.app.features.form.formEditableSquareAvatarItem
import io.yoosh.app.features.form.formMultiLineEditTextItem
import io.yoosh.app.features.form.formSwitchItem
import io.yoosh.app.features.home.AvatarRenderer
import io.yoosh.app.features.roomprofile.settings.RoomSettingsViewState
import io.yoosh.app.features.settings.AppPreferences
import org.matrix.android.sdk.api.session.room.model.RoomJoinRules
import org.matrix.android.sdk.api.util.toMatrixItem
import javax.inject.Inject

class SpaceSettingsController @Inject constructor(
        private val stringProvider: StringProvider,
        private val avatarRenderer: AvatarRenderer,
        private val vectorPreferences: AppPreferences,
        private val dimensionProvider: DimensionProvider
) : TypedEpoxyController<RoomSettingsViewState>() {

    interface Callback {
        fun onAvatarDelete()
        fun onAvatarChange()
        fun onNameChanged(name: String)
    }

    var callback: Callback? = null

    override fun buildModels(data: RoomSettingsViewState?) {
        val roomSummary = data?.roomSummary?.invoke() ?: return
        val host = this

        verticalMarginItem {
            id("margin-1")
            heightInPx(host.dimensionProvider.getDimension(R.dimen.layout_vertical_margin).toInt())
        }

        formEditableSquareAvatarItem {
            id("avatar")
            spaceTitle(roomSummary.displayName)
            enabled(data.actionPermissions.canChangeAvatar)
            when (val avatarAction = data.avatarAction) {
                RoomSettingsViewState.AvatarAction.None -> {
                    // Use the current value
                    avatarRenderer(host.avatarRenderer)
                    // We do not want to use the fallback avatar url, which can be the other user avatar, or the current user avatar.
                    matrixItem(roomSummary.toMatrixItem().updateAvatar(data.currentRoomAvatarUrl))
                }
                RoomSettingsViewState.AvatarAction.DeleteAvatar ->
                    imageUri(null)
                is RoomSettingsViewState.AvatarAction.UpdateAvatar ->
                    imageUri(avatarAction.newAvatarUri)
            }
            clickListener { host.callback?.onAvatarChange() }
            deleteListener { host.callback?.onAvatarDelete() }
        }

        verticalMarginItem {
            id("margin-2")
            heightInPx(host.dimensionProvider.getDimension(R.dimen.layout_vertical_margin_big).toInt())
        }

        formEditTextItem {
            id("name")
            enabled(data.actionPermissions.canChangeName)
            value(data.newName ?: roomSummary.displayName)
            hint(host.stringProvider.getString(R.string.create_space_name_hint))
            onTextChange { text ->
                host.callback?.onNameChanged(text)
            }
        }

    }
}
