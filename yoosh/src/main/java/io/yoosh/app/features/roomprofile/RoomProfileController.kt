/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.yoosh.app.features.roomprofile

import com.airbnb.epoxy.TypedEpoxyController
import io.yoosh.app.R
import io.yoosh.app.core.epoxy.ItemPosition
import io.yoosh.app.core.epoxy.expandableTextItem
import io.yoosh.app.core.epoxy.profiles.buildProfileAction
import io.yoosh.app.core.epoxy.profiles.buildProfileSection
import io.yoosh.app.core.resources.DimensionProvider
import io.yoosh.app.core.resources.StringProvider
import io.yoosh.app.core.ui.list.genericFooterItem
import io.yoosh.app.features.home.ShortcutCreator
import io.yoosh.app.features.home.room.detail.timeline.TimelineEventController
import io.yoosh.app.features.home.room.detail.timeline.tools.createLinkMovementMethod
import io.yoosh.app.features.settings.AppPreferences
import org.matrix.android.sdk.api.crypto.RoomEncryptionTrustLevel
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import javax.inject.Inject

class RoomProfileController @Inject constructor(
        private val stringProvider: StringProvider,
        private val shortcutCreator: ShortcutCreator,
        private val dimensionProvider: DimensionProvider
) : TypedEpoxyController<RoomProfileViewState>() {

    var callback: Callback? = null

    interface Callback {
        fun onLearnMoreClicked()
        fun onMemberListClicked()
        fun onUploadsClicked()
        fun createShortcut()
        fun onSettingsClicked()
        fun onLeaveRoomClicked()
    }

    override fun buildModels(data: RoomProfileViewState?) {
        data ?: return
        buildProfileAction(
                id = "settings",
                title = stringProvider.getString(R.string.room_profile_section_more_settings),
                icon = R.drawable.ic_room_profile_settings,
                action = { callback?.onSettingsClicked() },
                dimensionProvider = dimensionProvider,
                position = ItemPosition.FIRST
        )
        buildProfileAction(
                id = "member_list",
                title = stringProvider.getString(R.string.members),
                icon = R.drawable.ic_room_profile_member_list,
                action = { callback?.onMemberListClicked() },
                dimensionProvider = dimensionProvider,
                position = ItemPosition.INBETWEEN
        )
        buildProfileAction(
                id = "uploads",
                title = stringProvider.getString(R.string.room_profile_section_more_uploads),
                icon = R.drawable.ic_cloud_upload,
                action = { callback?.onUploadsClicked() },
                dimensionProvider = dimensionProvider,
                position = ItemPosition.INBETWEEN
        )
        if (shortcutCreator.canCreateShortcut()) {
            buildProfileAction(
                    id = "shortcut",
                    title = stringProvider.getString(R.string.room_settings_add_homescreen_shortcut),
                    editable = false,
                    icon = R.drawable.ic_add_to_home_screen_24dp,
                    action = { callback?.createShortcut() },
                    dimensionProvider = dimensionProvider,
                    position = ItemPosition.INBETWEEN
            )
        }
        buildProfileAction(
                id = "leave",
                title = stringProvider.getString(R.string.delete),
                divider = false,
                destructive = true,
                icon = R.drawable.ic_room_actions_leave,
                editable = false,
                action = { callback?.onLeaveRoomClicked() },
                dimensionProvider = dimensionProvider,
                position = ItemPosition.LAST
        )
    }

}
