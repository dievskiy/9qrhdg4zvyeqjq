/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.spaces

import com.airbnb.epoxy.EpoxyController
import io.yoosh.app.features.home.AvatarRenderer
import io.yoosh.app.space
import org.matrix.android.sdk.api.session.group.model.GroupSummary
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.RoomType
import org.matrix.android.sdk.api.util.toMatrixItem
import javax.inject.Inject

class SpaceHomeSummaryController @Inject constructor(
        private val avatarRendererVar: AvatarRenderer) : EpoxyController() {

    var callback: Callback? = null
    private var viewState: SpaceListViewState? = null

    init {
        requestModelBuild()
    }

    fun update(viewState: SpaceListViewState) {
        this.viewState = viewState
        requestModelBuild()
    }

    override fun buildModels() {
        val nonNullViewState = viewState ?: return
        val selectedGroupingMethod = viewState?.selectedGroupingMethod?.space()

        buildGroupModels(
                nonNullViewState.asyncSpaces(),
                selectedGroupingMethod)
    }

    private fun buildGroupModels(summaries: List<RoomSummary>?,
                                 selectedGroupingMethod: RoomSummary?) {
        var zeroSpaces = false
        selectedGroupingMethod?.let { group ->
            val grouping = summaries?.find {  it.roomId == group.roomId }  ?: return@let
            val spaces =
                    grouping.spaceChildren
                            ?.filter { it.roomType == RoomType.SPACE } ?: return@let

            if (spaces.isEmpty()) zeroSpaces = true
            spaces.forEach { spaceChildInfo ->
                val matrixItem = spaceChildInfo.toMatrixItem()
                spaceSummaryItemHome {
                    avatarRenderer(this@SpaceHomeSummaryController.avatarRendererVar)
                    id(spaceChildInfo.childRoomId)
                    matrixItem(matrixItem)
                    listener { this@SpaceHomeSummaryController.callback?.onSpaceSelected(summaries.first { it.roomId == spaceChildInfo.childRoomId }) }
                    longClickListener {
                        summaries.first { it.roomId == spaceChildInfo.childRoomId }.let {
                            this@SpaceHomeSummaryController.callback?.onSpaceSettings(it)
                        }
                    }
                }
            }
        } ?: run {
            // then we are on the root level
            val rootSpaces = viewState?.rootSpacesOrdered
            if (rootSpaces?.size == 0) zeroSpaces = true
            rootSpaces?.forEach { roomSummary ->
                spaceSummaryItemHome {
                    avatarRenderer(this@SpaceHomeSummaryController.avatarRendererVar)
                    id(roomSummary.roomId)
                    matrixItem(roomSummary.toMatrixItem())
                    listener { this@SpaceHomeSummaryController.callback?.onSpaceSelected(roomSummary) }
                    longClickListener { this@SpaceHomeSummaryController.callback?.onSpaceSettings(roomSummary) }
                }
            }
        }

        callback?.zeroSpaces(zeroSpaces)
    }

    interface Callback {
        fun onSpaceSelected(spaceSummary: RoomSummary?)
        fun onSpaceInviteSelected(spaceSummary: RoomSummary)
        fun onSpaceSettings(spaceSummary: RoomSummary)
        fun onToggleExpand(spaceSummary: RoomSummary)

        fun onGroupSelected(groupSummary: GroupSummary?)
        fun zeroSpaces(zeroSpaces: Boolean)
    }
}
