/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.matrixto

import androidx.core.view.isVisible
import io.yoosh.app.R
import io.yoosh.app.core.extensions.setTextOrHide
import io.yoosh.app.core.resources.StringProvider
import io.yoosh.app.databinding.FragmentMatrixToRoomSpaceCardBinding
import io.yoosh.app.features.home.AvatarRenderer
import io.yoosh.app.features.home.room.detail.timeline.TimelineEventController
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.SpaceChildInfo
import org.matrix.android.sdk.api.session.user.model.User
import org.matrix.android.sdk.api.util.toMatrixItem
import javax.inject.Inject

class SpaceCardRenderer @Inject constructor(
        private val avatarRenderer: AvatarRenderer,
        private val stringProvider: StringProvider
) {

    fun render(spaceSummary: RoomSummary?,
               inCard: FragmentMatrixToRoomSpaceCardBinding) {
        if (spaceSummary == null) {
            inCard.matrixToCardContentVisibility.isVisible = false
            inCard.matrixToCardButtonLoading.isVisible = true
        } else {
            inCard.matrixToCardContentVisibility.isVisible = true
            inCard.matrixToCardButtonLoading.isVisible = false
            avatarRenderer.render(spaceSummary.toMatrixItem(), inCard.matrixToCardAvatar)
            inCard.matrixToCardNameText.text = spaceSummary.name
            val memberCount = spaceSummary.otherMemberIds.size
            if (memberCount != 0) {
                inCard.spaceChildMemberCountText.text = stringProvider.getQuantityString(R.plurals.room_title_members, memberCount, memberCount)
            }
        }
    }

    fun render(spaceChildInfo: SpaceChildInfo?,
               inCard: FragmentMatrixToRoomSpaceCardBinding) {
        if (spaceChildInfo == null) {
            inCard.matrixToCardContentVisibility.isVisible = false
            inCard.matrixToCardButtonLoading.isVisible = true
        } else {
            inCard.matrixToCardContentVisibility.isVisible = true
            inCard.matrixToCardButtonLoading.isVisible = false
            avatarRenderer.render(spaceChildInfo.toMatrixItem(), inCard.matrixToCardAvatar)
            inCard.matrixToCardNameText.setTextOrHide(spaceChildInfo.name)
            val memberCount = spaceChildInfo.activeMemberCount ?: 0
            if (memberCount != 0) {
                inCard.spaceChildMemberCountText.text = stringProvider.getQuantityString(R.plurals.room_title_members, memberCount, memberCount)
            }
        }
    }
}
