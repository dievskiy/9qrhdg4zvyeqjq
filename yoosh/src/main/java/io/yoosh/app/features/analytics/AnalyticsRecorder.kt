package io.yoosh.app.features.analytics

import android.view.View

interface AnalyticsRecorder {
    fun recordViewTouched(view: View)
    fun recordEvent(event: AnalyticsEvent.Basic)
    fun toggleAnalytics(enabled: Boolean)
    fun recordCustomEvent(name: String, values: Map<String, String>? = null)
}
